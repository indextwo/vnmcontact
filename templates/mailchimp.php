﻿<?php

$formGroupArray = get_field('mailchimp-form', $formID);

//	Enqueue the custom MailChimp validation script (with our custom JS events for success handling) and our JS MC handling script

wp_enqueue_script('vnm-mailchimp', $scriptURI . 'mc-validate.js', array('vnm-form-globals'), filemtime($scriptPath . 'mc-validate.js'), true);

wp_enqueue_script('vnm-contact-mailchimp', $scriptURI . 'mailchimp.js', array('vnm-mailchimp'), filemtime($scriptPath . 'mailchimp.js'), true);

wp_localize_script('vnm-contact-mailchimp', 'formsObject', $formsObject);

//	Inject the inline MailChimp JS directly into the page

$mailChimpJS = $formGroupArray['mailchimp-js'];

if ($mailChimpJS) {
	wp_add_inline_script('jquery', $mailChimpJS);
}

//	Leadgen Success - form response triggers

wp_enqueue_script('vnm-leadgen-success', $scriptURI . 'leadgen-success.js', array('vnm-contact-mailchimp'), filemtime($scriptPath . 'leadgen-success.js'), true);

//	HTML Output

?>

<div class="vnm-contact-form-wrapper-internal">
	<?php echo $formGroupArray['mailchimp-html']; ?>
</div>