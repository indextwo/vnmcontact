﻿<?php

$formGroupArray = get_field('convertr-form', $formID);

$formsObject['convertr-script-src']	= $formGroupArray['convertr-script-src'];
$formsObject['convertr-script-id']	= $formGroupArray['convertr-script-id'];
// $formsObject['convertr-data-url']	= $formGroupArray['convertr-data-url'];

//	Enqueue Convertr remote jS

wp_enqueue_script('vnm-convertr', $formsObject['convertr-script-src'], array('vnm-form-globals'), '1.0', true);

wp_enqueue_script('vnm-contact-convertr', $scriptURI . 'convertr.js', array('vnm-convertr'), filemtime($scriptPath . 'convertr.js'), true);

wp_localize_script('vnm-contact-convertr', 'formsObject', $formsObject);

//	NOTE! This does not work the way it appears. We're adding META-data to the script. It's then hooked into with the script_loader_tag filter in vnmContact.php

// wp_script_add_data('vnm-convertr' , 'cvr-data-url' , $formsObject['convertr-data-url']);
wp_script_add_data('vnm-convertr' , 'cvr-data-id' , $formsObject['convertr-script-id']);

//	Inject the inline convertr JS directly into the page

//	Leadgen Success - form response triggers

wp_enqueue_script('vnm-leadgen-success', $scriptURI . 'leadgen-success.js', array('vnm-contact-convertr'), filemtime($scriptPath . 'leadgen-success.js'), true);

?>

<div id="cvtr-form-container"></div>