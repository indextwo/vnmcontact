﻿<?php

$formGroupArray = get_field('marketo-form', $formID);

// echo '<pre>';
// print_r($formGroupArray);
// echo '</pre>';

$formsObject['marketo_domain']		= $formGroupArray['marketo-form-domain'];
$formsObject['marketo_munchkin']	= $formGroupArray['marketo-munchkin-id'];
$formsObject['marketo_form']		= $formGroupArray['marketo-form-id'];
$formsObject['marketo_input']		= $formGroupArray['marketo-whitepaper-field'];
$formsObject['response']			= ($ajaxResponse) ? $ajaxResponse : $formGroupArray['marketo-response'];

//	Load the remote Marketo script

wp_enqueue_script('vnm-marketo', '//' . $formsObject['marketo_domain'] . '/js/forms2/js/forms2.min.js', array('vnm-form-globals'), '1.0', true);

//	Local Marketo script to deal with form handling

wp_enqueue_script('vnm-contact-marketo', $scriptURI . 'marketo.js', array('vnm-marketo'), filemtime($scriptPath . 'marketo.js'), true);

wp_localize_script('vnm-contact-marketo', 'formsObject', $formsObject);

//	Leadgen Success - form response triggers

wp_enqueue_script('vnm-leadgen-success', $scriptURI . 'leadgen-success.js', array('vnm-contact-marketo'), filemtime($scriptPath . 'leadgen-success.js'), true);

?>

<div class="vnm-contact-form-wrapper-internal"></div>