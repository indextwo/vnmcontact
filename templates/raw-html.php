﻿<?php

$formGroupArray = get_field('rawhtml-form', $formID);

//	Directly spit out the raw HTML output

?>

<div class="vnm-contact-form-wrapper-internal">
	<?php echo $formGroupArray['raw-html']; ?>
</div>