﻿<?php

$formGroupArray = get_field('standard-form', $formID);
$formFieldsArray = $formGroupArray['fields'];

//	Do we need Google Captcha?

if ($formGroupArray['has-captcha']) {
	wp_enqueue_script('vnmcontact-captcha-script');
}

wp_register_script('vnm-contact-datepicker', $scriptURI . 'DateTimePicker.js', array(), filemtime($scriptPath . 'DateTimePicker.js'), true);
wp_register_style('vnm-contact-datepicker-css', $cssURI . 'DateTimePicker.css');
	
//	Do we need a datepicker?

$datePickerEnqueued = false;

foreach ($formFieldsArray as $fieldArray) {
	if (!$datePickerEnqueued) {
		if ($fieldArray['field-type'] == 'datepicker') {
			wp_enqueue_script('vnm-contact-datepicker');
			wp_enqueue_style('vnm-contact-datepicker-css');
			
			wp_localize_script('vnm-form-validation', 'DatePickerObject', array(
				'hasDatePicker' => true,
			));
			
			$datePickerEnqueued = true;
		}
	}
}

//	Output the form
	
$nonce = wp_create_nonce('contact-nonce');

$uniqueFormID = bin2hex(uniqid(rand(), true));
	
$formAttrs = 'data-uniqueid="' . $uniqueFormID . '" data-sendaction="vnm_contact_form_sending" data-successaction="vnm_contactform_success" data-failaction="vnm_contact_form_error"';

if (!$formGroupArray['ajax-post'] && $formGroupArray['post-action']) {
	$formAttrs = 'action="' . admin_url('admin-post.php') . '" method="post"';
}

//	HTML output

?>
	
<?php do_action('vnm_contact_before_form', $formID, $attributes); ?>

<form id="vnm-form-<?php echo $uniqueFormID; ?>" class="vnmcontactform validate-form form relative" <?php do_action('vnm_contact_form_action'); ?> <?php echo $formAttrs; ?> data-fielderror="<?php _e('This field is required', 'vnmContact'); ?>">
	
	<input type="hidden" id="nonce" data-id="nonce" value="<?php echo $nonce; ?>" />
	
	<?php if ($formGroupArray['ajax-post']) : ?>
		<input type="hidden" id="form-action" data-id="form-action" value="vnmContactSend" />
	<?php endif; ?>
	
	<?php if (!$formGroupArray['ajax-post'] && $formGroupArray['post-action']) : ?>
		<input type="hidden" name="action" value="<?php echo $formGroupArray['post-action']; ?>" />
	<?php endif; ?>
	
	<?php if ($successRedirect) : ?>
		<input type="hidden" name="complete" value="<?php echo $successRedirect; ?>" />
	<?php endif; ?>
	
	<span class="formfield hidden">
		<input type="hidden" id="uniqueid-<?php echo $uniqueFormID; ?>" data-id="uniqueid" name="uniqueid" class="inputfield" value="<?php echo $uniqueFormID; ?>" />
	</span>
	
	<span class="formfield hidden">
		<input type="hidden" id="formslug-<?php echo $uniqueFormID; ?>" data-id="formslug" name="formslug" class="inputfield" value="<?php echo $formID; ?>" />
	</span>
	
	<?php
		///
		//	The fields
		///
	?>
	
	<div class="boxcontainer">
		<?php
			foreach ($formFieldsArray as $fieldArray) {
				echo vnmContact_returnField($fieldArray['field-type'], $uniqueFormID, $fieldArray);
			}
		?>
		
		<?php
			///
			//	'Fax' field - not to be filled out!
			///
		?>
		
		<div class="form-row formfield box six hide">
			<label class="block" for="fax">
				<?php _e('Do not fill out this field!', 'vnmContact'); ?>
			</label>
			
			<input id="fax-<?php echo $uniqueFormID; ?>" data-id="fax" name="fax" type="text" class="input-text inputfield block size-0938" placeholder="<?php _e('Do not fill out this field!', 'vnmContact'); ?>" autocomplete="off" />
		</div>
		
		<?php
			///
			//	Google Captcha
			///
		?>
		
		<?php if ($formGroupArray['has-captcha']) : ?>
			
			<div class="captcha-wrapper formfield form-row box twelve">
				<input type="hidden" data-id="g-recaptcha-response" id="g-recaptcha-response-<?php echo $uniqueFormID; ?>" class="field inputfield required" value="" data-error="<?php _e('Please check the \'I am not a robot\' field to confirm that you are human.', 'vnmContact'); ?>" />
				
				<div class="g-recaptcha inlineblock" data-sitekey="<?php the_field('vnmcontact-captcha-site-key', 'option'); ?>" data-callback="vnmContact_confirmCaptcha"></div>
			</div>
			
		<?php endif; ?>
	</div>
	
	<?php do_action('vnm_contact_before_submit_button', $formID, $attributes); ?>
	
	<?php
		
		///
		//	Submit Button
		///
		
		$buttonClasses = apply_filters('vnm_contact_button_classes', 'button secondarycolorbg darkgreybghover white whitehover mobilefullwidth bold size-1 block ease fullwidth');
	?>
	
	<div class="button-wrapper">
		<button type="submit" class="<?php echo $buttonClasses; ?>"><?php _ex('Send', 'Submit form', 'vnmContact'); ?></button>
	</div>
	
	<?php
		///
		//	Validation errors
		///
	?>
	
	<div class="validation-error aligncenter errorcolor semibold padding-1" data-error="<?php _e('Please correct all form errors before proceeding.', 'vnmContact'); ?>"></div>
	
</form>

<div id="form-response-<?php echo $uniqueFormID; ?>" class="size-1 hide"></div>

<?php

//	Enqueue the form validation script

wp_enqueue_script('vnm-form-validation', $scriptURI . 'form-validation.js', array('jquery'), filemtime($scriptPath . 'form-validation.js'), true);

?>