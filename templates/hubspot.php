﻿<?php

$formGroupArray = get_field('hubspot-form', $formID);

$formsObject['hubspot_portal_id']	= $formGroupArray['hubspot-portal-id'];
$formsObject['hubspot_form_id']		= $formGroupArray['hubspot-form-id'];
$formsObject['hubspot_campaign_id']	= $formGroupArray['marketo-campaign-id'];

//	Enqueue Hubspot remote jS

wp_enqueue_script('vnm-hubspot', '//js.hsforms.net/forms/v2.js', array('vnm-form-globals'), '1.0', true);

wp_enqueue_script('vnm-contact-hubspot', $scriptURI . 'hubspot.js', array('vnm-hubspot'), filemtime($scriptPath . 'hubspot.js'), true);

wp_localize_script('vnm-contact-hubspot', 'formsObject', $formsObject);

//	Inject the inline hubspot JS directly into the page

//	Leadgen Success - form response triggers

wp_enqueue_script('vnm-leadgen-success', $scriptURI . 'leadgen-success.js', array('vnm-contact-hubspot'), filemtime($scriptPath . 'leadgen-success.js'), true);

?>