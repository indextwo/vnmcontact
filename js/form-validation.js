var vnmContact_confirmCaptcha;

jQuery(function($) {
	
	var checkTimeout;
	var timeoutObject = new Object();
	var errorWrapper = '<div class="box errormessage nomargin"></div>';
	
	var robotField = $('.vnmcontactform [data-id="fax"]');
	
	robotField.attr('autocomplete', 'off');
	robotField.val('');
	
	///
	//	'Live' error-checking: based on the datatype of the field, 
	//	this will discern an error during input, paste, change, or focus-out
	///
	
	$('.vnmcontactform').on('input paste blur change', 'input, textarea, select', function(e) {
		window.clearTimeout(checkTimeout);
		
		var field = $(this);
		var dataID = field.attr('data-id');
		var parent = field.closest('.formfield');
		
		//	Set the error message
		
		var errorMessage = $('.vnmcontactform').attr('data-fielderror');
		
		if (field.attr('data-message')) {
			errorMessage = field.attr('data-message');
		}
		
		//	First let's check for blank values on .required fields
		
		if (field.hasClass('required') || parent.hasClass('required')) {
			var fieldInvalid = field.val().length < 1;
			
			if (field.attr('type') == 'radio' || field.attr('type') == 'checkbox') {
				if (field.attr('type') == 'checkbox') {
					fieldInvalid = !field.is(':checked');	//	Field checked is false, so fieldInvalid is TRUE
				}
				
				if (field.attr('type') == 'radio') {
					fieldInvalid = !parent.find('input[data-id="' + dataID + '"]:checked').length;	//	Number of checked radio buttons is negative (0), so fieldInvalid is TRUE
				}
			}
			
			if (fieldInvalid) {	// 
				showError(field, parent, errorMessage);
			} else {
				clearError(field, parent);
			}
		}
		
		//	Now we're going to set a _delay variable based on the event (e) type or the field data-type
		//	This comes AFTER the `.required` field check, as we need to check email validity after the basic .required check
		
		var _delay = 1000;
		
		if (e.type == 'focusout') {
			switch (field.attr('data-type')) {
				case 'email': {
					if (!checkEmail(field.val())) {
						showError(field, parent, errorMessage);
					}
					break;
				}
				
				case 'num': {
					field.val(field.val().replace(/[^0-9]/g, ''));
					break;
				}
			}
			
			_delay = 0;
		}
		
		if (field.attr('data-type') == 'num') {
			_delay = 0;
		}
		
		//	In case we want to hook in to the validation triggers
		
		var validationResponse = $('body').triggerHandler('vnm_contact_inline_validation_field', [$(this)]);
		
		//	Now let's run a switch on those data-type fields
		//	IMPORTANT CAVEAT: If this is being triggered on the initial `blur` check, every field's timeout will be cleared EXCEPT THE LAST ONE;
		//	hence why we have that earlier switch-case check
		
		checkTimeout = window.setTimeout(function() {
			
			switch (field.attr('data-type')) {
				case 'email': {
					if (!checkEmail(field.val())) {
						showError(field, parent, errorMessage);
					} else {
						clearError(field, parent);
					}
					break;
				}
				
				case 'num': {
					field.val(field.val().replace(/[^0-9]/g, ''));
					break;
				}
			}
			
		}, _delay);
	});
	
	///
	//	Let's run an initial check on all of the current fields with values
	///
	
	$('form.vnmcontactform').find('input, select, textarea').each(function(index) {
		
		//	Only do it for non-button inputs, as they might have a value of 1, which would trigger warning messages on page load
		
		if ($(this).attr('type') != 'radio' && $(this).attr('type') != 'checkbox') {
			//console.log('Running an initial check on', $(this).attr('data-id'), '(which is a', $(this).get(0).tagName, ') where the value length is', $(this).val().length);
			//if ($(this).val().length > 0 || $(this).get(0).tagName == 'SELECT') {
			if ($(this).val().length > 0) {
				$(this).blur();
			}
		}
	});
	
	/////
	///
	//	FORM SUBMISSION
	///
	/////
	
	$(document).on('submit', 'form.vnmcontactform', function(e) {

		$(document.body).trigger('gate_form_submitted');

		var sendingAction = $(this).attr('data-sendaction');
		var successAction = $(this).attr('data-successaction');
		var failureAction = $(this).attr('data-failaction');
		
		//	Let's try to trigger those errors ON PORPOISE
		
		$(this).find('input, textarea, select').trigger('blur');
		
		if ($(this).find('.error').length) {
			
			$('body').trigger('vnm_validation_show_error', new Object());	//	Sending an empty object because we're not actually using anything in this action
			
			//	No dice.
			
			return false;
		}
		
		//	If this is a 'regular' form submission, then it won't have AJAX form-submit actions:
		//	If so, then we can just skip it
		
		var formAction = $(this).find('input#form-action').val();
		
		if (!formAction) {
			return true;
		}
		
		//	Otherwise, everything looks okay
		
		e.preventDefault();
		
		var sendData = {};
		sendData['action'] = formAction;
		sendData['nonce'] = $(this).find('input#nonce').val();
		
		$('form.vnmcontactform .formfield').each(function(index) {
			var formField = $(this);
			var field = $(this).find('.inputfield');
			
			//	JUST IN CASE it doesn't have an `.inputfield` class
			
			if (field.length < 1) {
				field = $(this).find('input, textarea');
			}
			
			var fieldName = field.attr('id');
			
			if (!fieldName || fieldName == '' || field.attr('data-id')) {
				fieldName = field.attr('data-id');
			}
			
			var fieldValue = field.val();
			
			//	Checkbox? (Radio button should be covered by previous condition, as a radio button's value should be set)
			
			if (field.attr('type') == 'checkbox') {
				if (field.is(':checked')) {
					fieldValue = 'true';
				} else {
					fieldValue = '';
				}
			}
			
			sendData[fieldName] = fieldValue;
		});
		
		//	Try sending the form
		
		$('body').trigger(sendingAction);
		
		$.ajax({
			url: ajaxObject.ajaxurl,
			type: 'POST',
			data: sendData,
			dataType: 'json',
			
			success:	function(data) {
				
				if (data.response == 'success') {
					
					$('body').trigger(successAction, [data.message]);
					
					$('body').trigger('vnm_contact_validation_success', [data]);
					
				} else {
					
					$('body').trigger(failureAction, [data.message]);
					
				}
				
			},
			
			error:		function(xhr, ajaxOptions, thrownError) {
				console.log(xhr.response);
				console.log(xhr.message);
				console.log(thrownError);
				
				$('body').trigger(failureAction, [thrownError]);
			}
		});
		
		return false;
	});
	
	///
	//	Page sending action
	///
	
	//vnm_contact_form_sending
	
	/////
	///
	//	ERROR HANDLING
	///
	/////
	
	function showError(field, container, msg) {
		if (!container.find('.errormessage').length) {
			container.append(errorWrapper);
		}
		
		container.addClass('error');
		container.find('.errormessage').text(msg);
		
		errorObject = new Object();
		errorObject.field = field
		errorObject.container = container
		errorObject.msg = msg;
		
		$('body').trigger('vnm_validation_show_error', errorObject);
	}
	
	///
	//	Remove the error
	///
	
	function clearError(field, container) {
		container.removeClass('error');
		container.find('.errormessage').remove();
		
		errorObject = new Object();
		errorObject.field = field
		errorObject.container = container
		
		$('body').trigger('vnm_validation_clear_error', errorObject);
	}
	
	///
	//	Check email validity
	///

	function checkEmail(e) {
		var emailfilter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		return emailfilter.test(e);
	}
	
	///
	//	Check for actual letters
	///

	function checkAlpha(e) {
		var alphaFilter = /^[a-zA-Z]+/;
		return alphaFilter.test(e);
	}
	
	///
	//	Date picker: https://nehakadam.github.io/DateTimePicker/
	///
	
	if (typeof DatePickerObject != 'undefined' && DatePickerObject.hasDatePicker == '1') {
		if ($('body').find('.datebox').length > 0) {
			
			//	We're only picking the *first* datebox, as it shouldn't matter if there are multipe forms on the page
			
			var dateBox = $('.datebox:first');
			console.log(dateBox);
			
			dateBox.DateTimePicker({
				'setButtonContent':		'check-nocircle',
				'clearButtonContent':	'close-nocircle'
			});
		}
	} else {
		//console.log('No datepicker here.');
	}
	
	/////
	///
	//	ERROR HANDLING
	///
	/////
	
	$(document.body).on('vnm_validation_show_error', function(evt, errorObject) {
		//	errorObject.msg = error message for field (may be generic)
		//	errorObject contains .field, .container, .msg
		
		$('.vnmcontactform .validation-error').text($('.vnmcontactform .validation-error').attr('data-error'));
	});
	
	$(document.body).on('vnm_validation_clear_error', function(evt, errorObject) {
		//	errorObject contains .field, .container
		
		$('.vnmcontactform .validation-error').empty();
	});
	
	///
	//	Validate reCAPTCHA
	//	Technically, if there are multiple forms on a page, this might cause an issue, but... *shruggies*
	///

	vnmContact_confirmCaptcha = function(response) {
		$('input[data-id="g-recaptcha-response"]').val(response);
	};
	
	///
	//	Form sending
	///
	
	$(document.body).on('vnm_form_sending', function(evt, data) {
		
		showLoadingBlocker(true);
		
	});
	
	//	Form success
	
	$(document.body).on('vnm_validation_success', function(evt, data) {
		
		var msg = data.message;
		var uniqueID = data.uniqueid;
		
		//console.log(data);
		
		$('#vnm-form-' + uniqueID).slideUp();
		$('body').trigger('vnm_page_send_done');
		
		$('#form-response-' + uniqueID).html(msg);
		$('#form-response-' + uniqueID).removeClass('hide');
		
	});
	
});