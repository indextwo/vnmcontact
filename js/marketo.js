jQuery(function($) {
	
	//	No formsObject, no deal
	
	if (typeof formsObject == 'undefined') {
		return;
	}
	
	/////
	///
	//	MARKETO
	//	Examples of how to modify a Marketo form: http://developers.marketo.com/rest-api/assets/forms/examples/
	///
	/////
	
	if (typeof formsObject.marketo_munchkin != 'undefined' && typeof formsObject.marketo_form != 'undefined') {
		
		if (formsObject.marketo_subdomain != '' && formsObject.marketo_munchkin != '' && formsObject.marketo_form != '') {
			
			//	Create a form element for the Marketo JS to populate
			
			$('.vnm-contact-form-wrapper-internal').html('<form id="mktoForm_' + formsObject.marketo_form + '" class="marketo-form" />');
			
			//	Now the form has been created, let's inject the Marketo form
			
			var marketoForm = MktoForms2.loadForm('//' + formsObject.marketo_domain, formsObject.marketo_munchkin, formsObject.marketo_form, function(form) {
				
				//	When the form has been submitted (i.e. button pressed), trigger a tracking event

				form.onSubmit(function() {
					$(document.body).trigger('gate_form_submitted');
				});

			});
			
			//	Remove any styling from the form
			
			marketoForm.whenRendered(function(formObj) {
				$('.marketo-form, .marketo-form *').removeAttr('style');
				$('.marketo-form').find('style').remove();
				$('.mktoClear').remove();
				
				$('#mktoForms2BaseStyle').remove();
				$('#mktoForms2ThemeStyle').remove();
				
				///
				//	NOW! We're going to loop through all the <style> elements within the page; if any contain `.mktoForm`, then we're going to erase that element
				///

				$('style').each(function(index, elem) {
					let styleBlock = $(this);
					let content = styleBlock.text();

					if (content.indexOf('.mktoForm') > -1) {
						//	Found an inline style block - let's kill it!

						styleBlock.remove();
					}
				});

				//	Put all the asterisks AFTER the label text
				
				$('.mktoLabel').each(function(index) {
					var asterisk = $(this).find('.mktoAsterix');
					
					if (asterisk.length) {
						asterisk.appendTo($(this));
					}
				});
				
				//	Now: for any checkboxes, we need to move the label from OUTSIDE the input, and replace the blank one(?!) inside the checkbox wrapper
				
				$('.mktoCheckboxList').each(function(index) {
					var _this = $(this);
					var wrapper = _this.closest('.mktoFieldWrap');
					
					var thisLabel = _this.find('label');
					var actualLabel = wrapper.find('.mktoLabel');
					
					if (thisLabel.text() == '') {
						thisLabel.remove();
						actualLabel.appendTo(_this);
					}
				});
				
				//	Prepopulate a hidden field input with the whitepaper name, if set
				
				if (typeof formsObject.marketo_input != 'undefined' && formsObject.marketo_input != '') {
					if (typeof formsObject.whitepaper != 'undefined' && formsObject.whitepaper != '') {
						formObj.vals({
							[formsObject.marketo_input]: formsObject.whitepaper
						});
					} else {
						console.log('Tried to add whitepaper value to `' + formsObject.marketo_input + '`, but no whitepaper value passed in shortcode.');
					}
				}
				
				//	Prepopulate any other custom fields that have been set at the whitepaper page level
				
				if (typeof formsObject.formfields != 'undefined' && formsObject.formfields !== null) {
					for (var key in formsObject.formfields) {
						var fieldObj = formsObject.formfields[key];
						//console.log(fieldObj['field-name'] + ': ' + fieldObj['field-value']);
						
						formObj.vals({
							[fieldObj['field-name']]: fieldObj['field-value']
						});
					}
				}
				
				//	Set the success return function
				
				handleMarketoReturn(formObj);
			});
			
			//	Handle a successful response
			
			function handleMarketoReturn(formObj) {
				formObj.onSuccess(function(values, followUpUrl) {
					
					//	Ignore form's configured followUpUrl
					
					$('body').trigger('leadgen_success');
					
					//	Prevent submission handler from doing anything else
					
					return false;
				});
			}
		} else {
			console.error('One of the required Marketo parameters is not available:');
			console.error('Subdomain: ' + formsObject.marketo_subdomain);
			console.error('Munchkin ID: ' + formsObject.marketo_munchkin);
			console.error('Form ID: ' + formsObject.marketo_form);
		}
	}
});