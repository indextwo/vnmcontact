jQuery(function($) {
	
	/////
	///
	//	MAILCHIMP
	///
	/////
	
	//	Add some classes so we can style the form
	
	$('.mc-field-group').each(function(index) {
		var checkBox = $(this).find('input[type="checkbox"]');
		
		if (checkBox.length) {
			checkBox.addClass('checkbox');
			
			var _label = $(this).find('label');
			
			if (_label.length) {
				_label.addClass('checkbox__label');
			}
		} else {
			var _label = $(this).find('label');
			
			if (_label.length) {
				_label.addClass('form__label');
			}
		}
	});
	
	//	Since we're using a 'custom' implementation of MailChimp's JS library (mc-validate.js) and added a custom trigger to `mce_success_cb`, let's hook into that for a successful response
	
	$(document.body).on('mailchimp_success_trigger', function(e) {
		$('body').trigger('leadgen_success');
	});
	
	//	Did MailChimp actually fail? If so, can we figure out how?
	
	$(document.body).on('mailchimp_fail_trigger', function(e, response) {
		
		var msg = response.msg;
		
		if (msg.indexOf('already subscribed') > -1) {
			$('body').trigger('leadgen_success');
		}
	});
});