jQuery(function($) {
	
	var hubspotObj = {};
	hubspotObj.target = '.vnm-contact-form-wrapper';
	hubspotObj.portalId = formsObject.hubspot_portal_id;
	hubspotObj.formId = formsObject.hubspot_form_id;
	
	//	Include a campaign ID, if any
	
	if (typeof formsObject.hubspot_campaign_id != 'undefined') {
		if (formsObject.hubspot_campaign_id != '') {
			hubspotObj.sfdcCampaignId = formsObject.hubspot_campaign_id;
		}
	}
	
	//	Include a redirect notice, if any
	
	if (typeof formsObject.redirect != 'undefined') {
		if (formsObject.redirect != '') {
			hubspotObj.redirectUrl = formsObject.redirect;
		}
	}
	
	//	Include a text response, if any. Note that this will NOT work if a redirect URL has been set
	//	https://developers.hubspot.com/docs/methods/forms/advanced_form_options
	
	if (typeof formsObject.redirect == 'undefined' || formsObject.redirect == '') {
		if (typeof formsObject.response != 'undefined') {
			if (formsObject.response != '') {
				hubspotObj.inlineMessage = getFormattedResponse();
			}
		}
	}
	
	hbspt.forms.create(hubspotObj);

	//	Listen for a form 'response' - technically Hubspot doesn't actually send any kind of event or trigger on success, so we're just listening for 'the form has validated and been submitted'

	$('.vnm-contact-form-wrapper').on('hsvalidatedsubmit', '.hs-form', function(e) {

		//console.log('VALIDATED!');

		$(document.body).trigger('gate_form_submitted');

		$('body').trigger('leadgen_success');
		
		//	Prevent submission handler from doing anything else
		
		//return false;
	});
	
});