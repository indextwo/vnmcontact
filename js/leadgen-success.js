jQuery(function($) {
	
	///
	//	Special skip-the-lead-gen hook for clicks
	///
	
	$('body').on('click', '[data-trigger="skip-leadgen"]', function(e) {
		$('body').trigger('leadgen_success');
	});

	//##
	//#	We can also REMOVE the leadgen with this: $('body').trigger('remove_cookie');
	//##

	/////
	///
	//	FORM TRIGGERS
	///
	/////
	
	$(document.body).on('leadgen_success', function() {

		//	Write a cookie to indicate that this user has completed the form already
		
		var disableCookies = false;

		if (typeof formOptionsObject != 'undefined' && typeof formOptionsObject.disableCookies != 'undefined') {
			if (formOptionsObject.disableCookies == '1') {
				disableCookies = true;
			}
		}

		if (!disableCookies) {
			Cookies.set('leadgen_complete', 'completed', { expires: parseInt(formOptionsObject.cookieExpiry), path: formOptionsObject.cookiePath });
		}
		
		//	Trigger the download, if set
		
		$('.vnm-contact-form-wrapper, .vnm-contact-form-intro, .vnm-contact-form-outro').slideUp();
		
		if (typeof formsObject.download != 'undefined') {
			if (formsObject.download != '') {
				
				var downloadLink;
				
				if ($.isNumeric(formsObject.download)) {
					downloadLink = formsObject.baseURL + 'download/' + formsObject.download;
				} else {
					//	Let's just assume it's a qualified link for now
					downloadLink = formsObject.download;
				}
				
				$(document.body).trigger('leadgen_success_track', [downloadLink]);

				//window.location.href = downloadLink;
				downloadFile(downloadLink);
			}
		}

		//	Refresh the download link in the header just in case? 

		maybeRefreshDownload();	//	Defined in globals.js

		//	For the sake of a QUICK fix, we're going to insert the thank-you message (if set).
		//	# IMPORTANT NOTE! # This may confilct with either HubSpot and/or the Unlock Bar (which is in /includes/acf-blocks/lock-bar-block.js); but we'll need to deal with that later

		//	Maybe show a response regardless
		
		var responseText = getFormattedResponse();
		
		if (responseText != '') {
			$('.vnm-contact-form-response').html(responseText);
		}
		
		//	Otherwise, if a redirect is set, let's go there
		
		if (typeof formsObject.redirect != 'undefined') {
			if (formsObject.redirect != '') {

				$(document.body).trigger('leadgen_success_track', [formsObject.redirect]);
				
				window.location.href = formsObject.redirect;
			}
		}

		//	If the form is for unlocking content, then let's unlock that content!

		$('body').trigger('maybe_unlock_content');
	});
	
	///
	//	Click trigger for Download button
	///
	
	$('#whitepaper-download-button').click(function(e) {
		e.preventDefault;
		
		var downloadSelect = $('#whitepaper-download');

		$(document.body).trigger('download_track', downloadSelect.val());
		
		window.location.href = downloadSelect.val();
	});

	///
	//	Force-download the file by creating it as a link on the fly with a `download` attribute
	///
	
	function downloadFile(url) {
		var fileName = url.split('/').pop();
		
		var downloadLink = document.createElement('a');
		downloadLink.setAttribute('target', '_blank');
		downloadLink.download = fileName;
		downloadLink.innerHTML = "Download File";
		
		console.log('Created download link');
		
		downloadLink.href = url
		downloadLink.onclick = destroyClickedElement;
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);
		
		downloadLink.click();
	}

	function destroyClickedElement(event) {
		//	Remove the link from the DOM
		
		document.body.removeChild(event.target);
	}

	///
	//	Check for URL parameters
	///

	function getUrlParameter(sParam) {
		var sPageURL = window.location.search.substring(1),
			sURLVariables = sPageURL.split('&'),
			sParameterName,
			i;

		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');

			if (sParameterName[0] === sParam) {
				return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
			}
		}
		
		return false;
	};

	///
	//	We can check for the `gatecomplete=1` parameter in the URL (even on the static site!) to determine if we should remove the gate
	///

	if (getUrlParameter('gatecomplete') == 1) {
		$('body').trigger('leadgen_success');
	}
});