var getFormattedResponse;
var maybeRefreshDownload;

jQuery(function($) {
	///
	//	Debug clear cookies
	///
	
	$('body').on('click', '.cookie-kill-cookie', function(e) {
		$('body').trigger('remove_cookie');
	});
	
	//	jQuery(document.body).trigger('remove_cookie');

	$(document).on('remove_cookie', 'body', function() {
		Cookies.remove('leadgen_complete', { path: formOptionsObject.cookiePath });
		console.log('`leadgen_complete` cookie wiped.');
	});

	/////
	///
	//	ALREADY GOT COOKIE? DO SOME THINGS!
	///
	////

	maybeRefreshDownload = function() {
		let downloadLink = $('.pre-header-right a.download');
		
		if (downloadLink.is('[data-secondary]')) {
			// console.log('Download link has secondary attr()');
			downloadLink.attr('href', downloadLink.attr('data-secondary'));

			if (downloadLink.attr('data-secondary').indexOf('#') > -1) {
				downloadLink.removeAttr('target');
			}
		}
	}

	if (Cookies.get('leadgen_complete') && Cookies.get('leadgen_complete') == 'completed') {
		// console.log('There`s a COOKIE!');

		maybeRefreshDownload();
	}

	///
	//	Try to catch any CTA button clicks to maybe launch the gate in a lightbox, if found
	///

	$('.btn, .download.spot--text').click(function(e) {
		var ctaLink = $(this).attr('href');
		
		if (ctaLink) {
			if (ctaLink.indexOf('#') === 0) {
				var lightboxTrigger = $('#' + ctaLink.replace('#', 'wp-form-trigger-'));
				if (lightboxTrigger.length) {
					e.preventDefault();
					lightboxTrigger.prop('checked', true).trigger('change');
				}
			}
		}
	});

	///
	//	Try to catch any button clicks to CLOSE the form in a lightbox
	///

	$('body').on('click', '.close-form', function(e) {
		var myLightboxClose = $(this).closest('.lightbox').find('.close-cross');
		myLightboxClose.trigger('click');
	});
	
	///
	//	Get a formatted response
	///
	
	getFormattedResponse = function() {
		
		var responseText = '';
		
		if (typeof formsObject.response != 'undefined') {
			if (formsObject.response != '') {
				responseText = formsObject.response;
				
				if (typeof formsObject.download != 'undefined') {
					if (formsObject.download != '') {
						
						console.log('Got download var from formsObject...');
						
						var downloadLink;
						
						if ($.isNumeric(formsObject.download)) {
							downloadLink = formsObject.baseURL + 'download/' + formsObject.download;
						} else {
							//	Let's just assume it's a qualified link for now
							downloadLink = formsObject.download;
						}
						
						responseText = responseText.formatUnicorn({ download: downloadLink });
					}
				}
				
				if (typeof formsObject.redirect != 'undefined') {
					if (formsObject.redirect != '') {
						console.log('Got redirect var from formsObject...');
						
						responseText = responseText.formatUnicorn({ success: formsObject.redirect });
					}
				}
			}
		}
		
		return responseText;
	}
});

///
//	JS sprintf function
//
//	Usage: 'We are not in {place} anymore.'.formatUnicorn({ place: 'Kansas' })
///

String.prototype.formatUnicorn = String.prototype.formatUnicorn || function () {
	"use strict";
	var str = this.toString();
	if (arguments.length) {
		var t = typeof arguments[0];
		var key;
		var args = ("string" === t || "number" === t) ?
			Array.prototype.slice.call(arguments)
			: arguments[0];
			
		for (key in args) {
			str = str.replace(new RegExp("\\{" + key + "\\}", "gi"), args[key]);
		}
	}

	return str;
};