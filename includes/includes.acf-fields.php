<?php

///
//	Add ACF Options page
///

if (function_exists('acf_add_options_page')) {
	acf_add_options_page(array(
		'page_title' 	=> 'Contact Form Settings',
		'menu_title'	=> 'Contact Form Settings',
		'menu_slug' 	=> 'vnm-contact-settings',
		'parent_slug'	=> 'edit.php?post_type=vnmcontactform',
		'capability'	=> 'manage_options',
		'redirect'		=> false
	));
}

function vnmContact_createACFGroups() {
	
	$postID = isset($_GET['post']) ? $_GET['post'] : 0;
	$postSlug = basename(get_permalink($postID));
	
	///
	//	VNM Contact Groups
	///
	

	$formTypeArray = array(
		'' => 'Choose form type',
		'standard'	=> 'Standard Form',
		'marketo'	=> 'Marketo Form',
		'hubspot'	=> 'HubSpot Form',
		'convertr'	=> 'Convertr Form',
		'mailchimp'	=> 'MailChimp Form',
		'raw-html'	=> 'Raw HTML',
	);

	$formTypeArray = apply_filters('vnm_contact_form_types', $formTypeArray);
	
	//echo '<pre style="padding-left:15rem;">';
	//print_r($formSectionsArray);
	//echo '</pre>';
	
	acf_add_local_field_group(array(
		'key' => 'group__acf_vnmcontact_contactforms',
		'title' => 'Contact Forms',
		'fields' => array(
			
			//	Select the type of contact form
			
			array(
				'key' => 'field__acf_vnmcontact_contactforms_type',
				'label' => 'Form Type',
				'name' => 'form-type',
				'type' => 'select',
				'instructions' => 'Select the type of form to use',
				'required' => 1,
				'conditional_logic' => 0,
				'choices' => $formTypeArray,
				'default_value' => array(
				),
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'return_format' => 'value',
				'ajax' => 0,
				'placeholder' => '',
			),
			
			///
			//	Form intro
			///
			
			array (
				'key' => 'field__acf_vnmcontact_contactforms_intro',
				'label' => 'Form intro',
				'name' => 'form-intro',
				'type' => 'wysiwyg',
				'instructions' => 'This text will be placed before the form, regardless of type. If the form has an ajax response, it will be collapsed along with the rest of the form upon receiving a response.',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 0,
			),
			
			///
			//	Form group includes
			///
			
			vnmContact_getACFFormType('standard'),
			
			vnmContact_getACFFormType('marketo'),
			
			vnmContact_getACFFormType('hubspot'),
			
			vnmContact_getACFFormType('convertr'),
			
			vnmContact_getACFFormType('mailchimp'),
			
			vnmContact_getACFFormType('raw-html'),
			
			///
			//	Any custom groups
			///
			
			do_action('vnm_contact_add_form_types'),	//	This needs to echo ACF field group arrays
			
			///
			//	Cookie response
			///

			array(
				'key' => 'field__acf_vnmcontact_contactforms_enable_cookie',
				'label' => 'Disable autowrite cookie?',
				'name' => 'disable-cookie',
				'type' => 'true_false',
				'instructions' => 'With this option on, this form will <strong>not</strong> automatically store the <code>leadgen_complete</code> cookie when the user completes the form.',
				'default_value' => 0,
				'ui' => 1,
				'ui_on_text' => 'Disable',
				'ui_off_text' => 'Default',
			),
			
			array(
				'key' => 'field__acf_vnmcontact_contactforms_cookie_message',
				'label' => '`Already Filled Out` Cookie Message',
				'name' => 'cookie-message',
				'type' => 'textarea',
				'instructions' => 'If user has already completed the form, this message will be displayed instead. Wrap the link for the successful download in a shortcode; e.g. <code>[successlink]continue to download[/successlink]</code>',
				'required' => 0,
				'new_lines' => 'wpautop',
				'rows' => 4,
			),

			array(
				'key' => 'field__acf_vnmcontact_contactforms_cookie_button',
				'label' => '`Already Filled Out` Download Button',
				'name' => 'cookie-button',
				'type' => 'text',
				'instructions' => 'Download button text. Leave blank if you don\'t want a button to appear.',
				'required' => 0,
			),

			///
			//	Form outro
			///
			
			array (
				'key' => 'field__acf_vnmcontact_contactforms_outro',
				'label' => 'Form outro',
				'name' => 'form-outro',
				'type' => 'wysiwyg',
				'instructions' => 'This text will be placed after the form, regardless of type. If the form has an ajax response, it will be collapsed along with the rest of the form upon receiving a response.',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 0,
			),
			
		),
		
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'vnmcontactform',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	///
	//	Contact Form instructions. These actually appear the the top of the page.
	///
	
	//	Set the message instructions
	
	$instructions = '<ol>
		<li>The main title is the admin-only title for reference.</li>';
	
	if ($postSlug) {
		//$instructions .= '<li>Slug: <code>' . $postSlug . '</code></li>';
		$instructions .= '<li>Shortcode: <code>[wpform id="' . $postSlug . '"]</code> (Note that it should have a <code>download</code> or <code>success</code> attribute to do anything on subsmission)</li>';
		$instructions .= '<li>Redirect to a successful page: <code>[wpform id="' . $postSlug . '" success=123]</code> (where <code>123</code> = post ID)</li>';
		$instructions .= '<li>Download document automatically on success: <code>[wpform id="' . $postSlug . '" download=456]</code> (where <code>456</code> = download ID)</li>';
		$instructions .= '<li>Show a different response on success: <code>[wpform id="' . $postSlug . '" download=456 response="Thanks! Download that document &lt;a href=\'{download}\'&gt;here&lt;/a&gt;"]</code></li>';
	}
	
	$instructions .= '</ol>';

	$instructions .= '<p>You can force-set the gate to behave as if the visitor has already completed it. Add <code>?gatecompleted=1</code> to the end of the URL and it will automatically set the gate cookie to <strong>completed</strong>.</p>';
	
	//	Add a message below the title so that we know what's what
	
	acf_add_local_field_group(array(
		'key' => 'group__acf_vnmcontact_contactforms_instructions',
		'title' => 'Please Note',
		'fields' => array(
			array(
				'key' => 'field__acf_vnmcontact_contactforms_instructions',
				'label' => '',
				'name' => '',
				'type' => 'message',
				'instructions' => '',
				'message' => $instructions,
				'new_lines' => 'wpautop',
				'esc_html' => 0,
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'vnmcontactform',
				),
			),
		),
		'position' => 'acf_after_title',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'active' => true,
	));
	
	///
	//	Plugin update settings
	///

	// acf_add_local_field_group(array (
	// 	'key' => 'group__acf_vnmcontact_plugin_settings_group',
	// 	'title' => 'Plugin Update',
	// 	'fields' => array (
	// 		array (
	// 			'key' => 'field__acf_vnmcontact_update_token',
	// 			'name' => 'vnmcontact-update-token',
	// 			'label' => 'App Password',
	// 			'type' => 'text',
	// 			'instructions' => 'The BitBucket app password to enable updates.',
	// 		),
	// 	),
	// 	'location' => array (
	// 		array (
	// 			array (
	// 				'param' => 'options_page',
	// 				'operator' => '==',
	// 				'value' => 'vnm-contact-settings',
	// 			),
	// 		),
	// 	),
	// 	'menu_order' => 0,
	// 	'position' => 'normal',
	// 	'style' => 'default',
	// 	'label_placement' => 'top',
	// 	'instruction_placement' => 'label',
	// 	'hide_on_screen' => '',
	// 	'active' => 1,
	// 	'description' => '',
	// ));

	///
	//	Google Captcha
	///
	
	acf_add_local_field_group(array (
		'key' => 'group__acf_vnmcontact_captcha_settings_group',
		'title' => 'Google Captcha Settings',
		'fields' => array (
			array (
				'key' => 'field__acf_vnmcontact_captcha_site_key',
				'name' => 'vnmcontact-captcha-site-key',
				'label' => 'Site Key',
				'type' => 'text',
				'instructions' => 'The user-facing public key.',
			),
			
			array (
				'key' => 'field__acf_vnmcontact_captcha_ssecret_key',
				'name' => 'vnmcontact-captcha-secret-key',
				'label' => 'Secret Key',
				'type' => 'text',
				'instructions' => 'The server-side secret key.',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'vnm-contact-settings',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));

	///
	//	Cookie Settings
	///
	
	acf_add_local_field_group(array (
		'key' => 'group__acf_vnmcontact_cookie_settings_group',
		'title' => 'Form Cookie Settings',
		'fields' => array (

			array(
				'key' => 'field__acf_vnmcontact_cookie_path',
				'label' => 'Form cookies: for multi-page or single page site?',
				'name' => 'vnmcontact-cookie-is-single-page',
				'type' => 'true_false',
				'instructions' => 'If this is a multi-page single site like an iView with a vanity domain, then this should be set to <strong>Multi-page</strong>. If the contact forms are going to be used on an individual basis like the Product CMS, where different IDC Products might share the same top-level domain, then this should be set to <strong>Single page</strong>.',
				'default_value' => 0,
				'ui' => 1,
				'ui_on_text' => 'Single Page',
				'ui_off_text' => 'Multi-page',
			),
			
			array (
				'key' => 'field__acf_vnmcontact_cookie-expiry',
				'name' => 'vnmcontact-cookie-expiry',
				'label' => 'Cookie Expiry',
				'type' => 'number',
				'default_value' => 90,
				'instructions' => 'Set the expiry of the form cookie in days. If left blank, this will default to <strong>90</strong>.',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'vnm-contact-settings',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	///
	//	Add as a Gutenberg BLock
	///
	
	acf_register_block(array(
		'name' => 'idc-gate-block',
		'title' => _x('IDC Gate', 'Name of block', 'vnmContact'),
		'description' => _x('Add a form gate', 'Description of block', 'vnmContact'),
		'render_template' => VNMCONTACT_PATH . '/includes/acf-blocks/contact-form-block.php',
		'icon' => array( 'src' => 'shield-alt', 'background' => '#2279BC' ),
	));
	
	///
	//	Gutenberg Block fields
	///
	
	acf_add_local_field_group(array(
		'key' => 'group__acf_vnmcontact_contact_block',
		'title' => 'Block: IDC Gate',
		'fields' => array(
			array(
				'key' => 'field__acf_vnmcontact_contact_form_post',
				'label' => 'Contact Form',
				'name' => 'idc-gate-form',
				'type' => 'post_object',
				'instructions' => 'Select the Contact Form to put in place. Form must already be defined in the <a href="/wp-admin/edit.php?post_type=vnmcontactform">Contact Forms</a> section.',
				'required' => 1,
				'conditional_logic' => 0,
				'post_type' => array(
					0 => 'vnmcontactform',
				),
				'return_format' => 'id',
				'ui' => 1,
			),

			array(
				'key' => 'field__acf_vnmcontact_unlock_content',
				'label' => 'Unlock page content or trigger download?',
				'name' => 'unlock-content',
				'type' => 'true_false',
				'instructions' => 'Should this form unlock the page content or trigger a download? If it unlocks page content, you also need to add a <strong>Lock Content Block</strong> in the appropriate place in the page.',
				'default_value' => 0,
				'ui' => 1,
				'ui_on_text' => 'Unlock Content',
				'ui_off_text' => 'Download',
			),

			array(
				'key' => 'field__acf_vnmcontact_unlock_content_response',
				'label' => '`Content successfully unlocked` response',
				'name' => 'unlock-response',
				'type' => 'textarea',
				'instructions' => 'This will be shown once the user has successfully completed the form.',
				'required' => 0,
				'new_lines' => 'wpautop',
				'rows' => 4,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field__acf_vnmcontact_unlock_content',
							'operator' => '==',
							'value' => '1',
						),
					),
				),
			),

			array(
				'key' => 'field__acf_vnmcontact_unlock_content_button',
				'label' => '`Content unlocked` button text',
				'name' => 'unlocked-button',
				'type' => 'text',
				'instructions' => 'This button will close the form lightbox.',
				'required' => 0,
				'default_value' => 'Continue reading',
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field__acf_vnmcontact_unlock_content',
							'operator' => '==',
							'value' => '1',
						),
					),
				),
			),
			
			array(
				'key' => 'field__acf_vnmcontact_contact_download_method',
				'label' => 'Download Method',
				'name' => 'download-method',
				'type' => 'select',
				'instructions' => 'Choose how to add the download triggered once the gate has been completed.',
				'choices' => array(
					'' => 'Choose method...',
					'upload' => 'Upload File',
					'url' => 'URL',
					'advanced' => 'Advanced',
				),
				'default_value' => '',
				'return_format' => 'value',
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field__acf_vnmcontact_unlock_content',
							'operator' => '!=',
							'value' => '1',
						),
					),
				),
			),

			array(
				'key' => 'field__acf_vnmcontact_download_file',
				'label' => 'Add File',
				'name' => 'download-success-file',
				'type' => 'file',
				'instructions' => 'Select a file for the download',
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field__acf_vnmcontact_contact_download_method',
							'operator' => '==',
							'value' => 'upload',
						),
						array(
							'field' => 'field__acf_vnmcontact_unlock_content',
							'operator' => '!=',
							'value' => '1',
						),
					),
				),
				'return_format' => 'url',
				'library' => 'all',
			),

			array(
				'key' => 'field__acf_vnmcontact_contact_download_url',
				'label' => 'Document URL',
				'name' => 'download-url',
				'type' => 'url',
				'instructions' => 'Add the URL to the download document',
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field__acf_vnmcontact_contact_download_method',
							'operator' => '==',
							'value' => 'url',
						),
						array(
							'field' => 'field__acf_vnmcontact_unlock_content',
							'operator' => '!=',
							'value' => '1',
						),
					),
				),
				'placeholder' => 'https://',
			),

			array(
				'key' => 'field__acf_vnmcontact_download_post',
				'label' => 'Download Monitor',
				'name' => 'download-success-post',
				'type' => 'post_object',
				'instructions' => 'Choose the Download file (it should already be set in the <a href="/wp-admin/edit.php?post_type=dlm_download">Downloads</a> section)',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field__acf_vnmcontact_contact_download_method',
							'operator' => '==',
							'value' => 'advanced',
						),
						array(
							'field' => 'field__acf_vnmcontact_unlock_content',
							'operator' => '!=',
							'value' => '1',
						),
					),
				),
				'post_type' => array(
					0 => 'dlm_download',
				),
				'taxonomy' => '',
				'allow_null' => 1,
				'multiple' => 0,
				'return_format' => 'id',
				'ui' => 1,
			),

			array(
				'key' => 'field__acf_vnmcontact_lightbox',
				'label' => 'Show in lightbox?',
				'name' => 'form-lightbox',
				'type' => 'true_false',
				'instructions' => 'Should this form display in a lightbox?',
				'default_value' => 0,
				'ui' => 1,
				'wrapper' => array(
					'width' => 20,
				),
			),

			array(
				'key' => 'field__acf_vnmcontact_form_unique_id',
				'label' => 'Unique Form ID',
				'name' => 'unique-id',
				'type' => 'text',
				'instructions' => 'If this form is in a lightbox, you need to give it a unique ID. alpha characters only, plus hyphens (<code>-</code>) &amp; underscores (<code>_</code>); no spaces.',
				'required' => 1,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field__acf_vnmcontact_lightbox',
							'operator' => '!=',
							'value' => '0',
						),
					),
				),
				'wrapper' => array(
					'width' => 40,
				),
				'prepend' => '#',
			),

			array(
				'key' => 'field__acf_vnmcontact_form_lightbox_title',
				'label' => 'Form Title',
				'name' => 'form-title',
				'type' => 'text',
				'instructions' => 'Enter a title to appear at the top of this form. For no title, leave this field blank.',
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field__acf_vnmcontact_lightbox',
							'operator' => '!=',
							'value' => '0',
						),
					),
				),
				'wrapper' => array(
					'width' => 40,
				),
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'block',
					'operator' => '==',
					'value' => 'acf/idc-gate-block',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));

	///
	//	Add a Gutenberg Block for the 'Lock Bar'
	///

	acf_register_block(array(
		'name' => 'idc-lock-bar',
		'title' => _x('Lock Content Block', 'Name of block', 'vnmContact'),
		'description' => _x('All content after this block will be locked until user has filled out the gate', 'Description of block', 'vnmContact'),
		'render_template' => VNMCONTACT_PATH . '/includes/acf-blocks/lock-bar-block.php',
		//'enqueue_style' => VNMCONTACT_URL . '/includes/acf-blocks/lock-bar-block.css',
		//'enqueue_script' => VNMCONTACT_URL . '/includes/acf-blocks/lock-bar-block.js',
		'enqueue_assets' => function() {
			wp_enqueue_style('lock-bar-block', VNMCONTACT_URL . '/includes/acf-blocks/lock-bar-block.css');
			wp_enqueue_script('lock-bar-block', VNMCONTACT_URL . '/includes/acf-blocks/lock-bar-block.js', array('jquery'), '', true );
		},
		'icon' => array( 'src' => 'hidden', 'background' => '#2279BC' ),
	));

	acf_add_local_field_group(array(
		'key' => 'group__acf_vnmcontact_lock_bar_block',
		'title' => 'Block: Lock Bar',
		'fields' => array(
			array(
				'key' => 'field__acf_vnmcontact_lock_bar_title',
				'label' => 'Title',
				'name' => 'title',
				'type' => 'text',
			),
			array(
				'key' => 'field__acf_vnmcontact_lock_bar_content',
				'label' => 'Content',
				'name' => 'content',
				'type' => 'wysiwyg',
				'tabs' => 'all',
				'toolbar' => 'minimalist',
				'media_upload' => 0,
			),
			array(
				'key' => 'field__acf_vnmcontact_lock_bar_link',
				'label' => 'Link',
				'name' => 'link',
				'type' => 'link',
				'instructions' => 'You should have already added a <strong>Gate</strong> set to open <em>in a lightbox</em> with a <strong>unique ID</strong>. Enter a hash (<code>#</code>) followed by the <strong>Unique Form ID</strong> for that gate; e.g. <code>#my_unique_gate</code>',
				'return_format' => 'array',
				'required' => 1,
			),
			array(
				'key' => 'field__acf_vnmcontact_lock_bar_formatting',
				'label' => 'Formatting',
				'name' => 'formatting',
				'type' => 'group',
				'layout' => 'block',
				'sub_fields' => array(
					array(
						'key' => 'field__acf_vnmcontact_lock_bar_bgcolor',
						'label' => 'Background Color',
						'name' => 'background_color',
						'type' => 'color_picker',
						'instructions' => 'Background colour for the block',
					),
					array(
						'key' => 'field__acf_vnmcontact_lock_bar_textcolor',
						'label' => 'Text Color',
						'name' => 'text_color',
						'type' => 'color_picker',
						'instructions' => 'Main text colour for the block',
					),

					array(
						'key' => 'field__acf_vnmcontact_lock_bar_topmargin',
						'label' => 'Top Margin',
						'name' => 'top_margin',
						'type' => 'number',
						'instructions' => 'Top margin in pixels. Can be a negative number.',
						'append' => 'px',
					),
				),
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'block',
					'operator' => '==',
					'value' => 'acf/idc-lock-bar',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));
}

add_action('acf/init', 'vnmContact_createACFGroups');

///
//	Get an include based on the form type (so we can keep everything nice and separated)
///

function vnmContact_getACFFormType($type) {
	
	if (file_exists(dirname(__FILE__) . '/acf-includes/form-type-' . $type . '.php')) {
		$formArray = include('acf-includes/form-type-' . $type . '.php');
		
		return $formArray;
	}
}

///
//	ACF field save
///

function vnmContact_acfSaveFields($postID) {
	
	//	No ACF data, or not a Partners post? No deal.
	
    if (empty($_POST['acf']) || get_post_type($postID) != 'vnmcontactform') {
        return;
    }
	
	$formType = get_field('form-type', $postID);
	
	//	If they're not using MailChimp, there's no need for sanitisation
	
	if ($formType != 'mailchimp') {
		return;
	}
	
	$mailChimpHTML = $mailingList['mailchimp-html'];
	$mailChimpJS = $mailingList['mailchimp-js'];
	
	//	If there's no markup to sanitize, bail out
	
	if (!$mailChimpHTML) {
		return;
	}
	
	libxml_use_internal_errors(true);
	
	$head = '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>';
	
	$doc = new DOMDocument();
	$doc->loadHTML($head . $mailChimpHTML);

	vnmContact_removeElementsByTagName('script', $doc);
	vnmContact_removeElementsByTagName('style', $doc);
	vnmContact_removeElementsByTagName('link', $doc);
	
	$cleanHTML = preg_replace('/<\/?(!doctype|html|head|meta|body)[^>]*>/im', '', $doc->saveHTML());
	
	libxml_use_internal_errors(false);
	
	//	Now let's use a 'simple' regex to remove script tags from the JS - this is because wp_add_inline_script doesn't like <script> tags
	
	$cleanJS = trim(preg_replace('#<script[^>]*>(.*)</script>#is', '$1', $mailChimpJS));
	$cleanJS = trim(str_replace('var $mcj = jQuery.noConflict(true);', '', $cleanJS));		//	This line specifically and unhelpfully removes all copies of `jQuery` from the global scope (thanks, MailChimp...)
	
	//	Add the updated fields to an array
	
	$htmlArray = array(
		'mailchimp-html'	=> $cleanHTML,
		'mailchimp-js'		=> $cleanJS,
	);
	
	update_field('mailinglist', $htmlArray, $postID);
}

add_action('acf/save_post', 'vnmContact_acfSaveFields', 20);	//	Priority set to 20 so we're hooking in AFTER it's been saved

///
//	Function for removing unwanted HTML elements
///

function vnmContact_removeElementsByTagName($tagName, $document) {
	$nodeList = $document->getElementsByTagName($tagName);
	
	for ($nodeIdx = $nodeList->length; --$nodeIdx >= 0; ) {
		$node = $nodeList->item($nodeIdx);
		$node->parentNode->removeChild($node);
	}
}

///
//	Add admin CSS for the Contact Form block
///

function vnmContact_enqueueBlockCSS() {
	$cssPath = VNMCONTACT_PATH . '/css/';
	$cssURI = VNMCONTACT_URL . '/css/';
	
	wp_enqueue_style('vnm-contact-block-css', $cssURI . 'vnmContact.css');
}

add_action('enqueue_block_editor_assets', 'vnmContact_enqueueBlockCSS', 10);

?>