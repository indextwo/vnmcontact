<?php

/*
Extension: VNM Contact Forms: Manage columns
Description: Adds custom info to the columns
Version: 0.1
Author: Lawrie Malen
Author URI: https://www.verynewmedia.com
License: GPL

Copyright (C) 2020 Lawrie Malen // Very New Media
Released 16-09-2020
Updated: 16-09-2020
*/

///
//	Add admin columns
///

function vnmContact_addColumns($columns) {
	
	$new_columns = array();
	
	foreach ($columns as $key => $value) {
		
		// default-ly add every original column
		$new_columns[$key] = $value;
		
		///
		//	If currently showing the title column, follow immediately with our custom columns
		///
		
		if ($key == 'title') {
			$new_columns['form_shortcode']	= __('Shortcode', 'vnmContact');
		}
	}
	
	return $new_columns;
}

add_filter('manage_vnmcontactform_posts_columns', 'vnmContact_addColumns');

///
//	Set values for custom columns
///

function vnmContact_manageColumns($column, $postID) {
	global $post;
	
	//echo '<h1 style="background: red; position: fixed; top: 0; left: 0; width: 100%; padding: 2rem; z-index: 999999;">' . $column . '/' . $postID . '</h1>';
	
	switch($column) {
		
		case 'form_shortcode' : {
			
			$slug = get_post_field('post_name', $postID);

			if (!$slug || $slug == '') {
				echo __('Not set', 'vnmFunctionality');
			} else {
				echo '<code>[wpform id="' . $slug . '"]</code>';
			}
			
			break;
		}
		
		default : {
			break;
		}
	}
}

add_action('manage_posts_custom_column', 'vnmContact_manageColumns', 10, 2);

///
//	Filter Games by whether they have started playing or not
///

function vnmContact_filterBy($postType, $which) {
	
	if ($postType == 'game') {
		
		$urlFilterArray = array(
			'no'	=> 'Not started', 
			'yes'	=> 'Started',
		);
		
		$currentFilter = '';
		
		if(isset($_GET['gamestarted'])) {
			$currentFilter = $_GET['gamestarted'];
		}
		
		$filterResult = ($currentFilter == 'yes') ? 'Games started' : 'Games not started';
		
		?>
		
		<select name="gamestarted" id="gamestarted">
			<option value="all" <?php selected('all', $currentFilter); ?>><?php _ex('All Games', 'admin', 'vnmGames'); ?></option>
			
			<?php foreach ($urlFilterArray as $key=>$value) { ?>
				<option value="<?php echo esc_attr($key); ?>" <?php selected($key, $currentFilter); ?>><?php echo esc_attr($value); ?></option>
			<?php } ?>
		</select>
		<?php
	}
}

//add_action('restrict_manage_posts', 'vnmContact_filterBy', 50, 2);

///
//	Parse the filtered query 
///

function vnmContact_parseFilterByStoreValue($query) {
	global $pagenow;
	$postType = 'game';
	
	$getPostType = isset($_GET['post_type']) ? $_GET['post_type'] : '';
	
	if ($getPostType != $postType) {
		return $query;
	}
	
	if (!is_admin() || $pagenow != 'edit.php') {
		return $query;
	}
	
	$queryArray = array();
	
	//	If we're filtering by Games that HAVE NOT started:
	
	if ($_GET['gamestarted'] == 'no') {
		$queryArray = array(
			'relation'		=> 'OR',
			array(
				'key'		=> 'game-started',
				'value'		=> '',
				'compare'	=> '='
			),
			array(
				'key'		=> 'game-started',
				'compare'	=> 'NOT EXISTS'
			),
		);
	}
	
	//	Otherwise, we're filtering by Games that HAVE started
	
	if ($_GET['gamestarted'] == 'yes') {
		$queryArray = array(
			array(
				'key'		=> 'game-started',
				'value'		=> '',
				'compare'	=> '!='
			),
		);
	}
	
	if (!empty($queryArray)) {
		$query->query_vars['meta_query'] = $queryArray;
	}
	
	return $query;
}

//add_filter('parse_query', 'vnmContact_parseFilterByStoreValue');

?>