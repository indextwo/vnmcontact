<?php

///
//	Ajax form submission
//	This is only for 'standard' forms - integrations like Marketo, MailChimp etc. don't make use of it
///

function vnmContact_checkSend() {
	
	//	Is the Google reCaptcha valid?
	
	if ($_POST['g-recaptcha-response']) {
		$captchaPost = sanitize_text_field($_POST['g-recaptcha-response']);
		
		$captchaSecretKey = get_field('vnmcontact-captcha-secret-key', 'option');
		
		$captchaResponse = wp_remote_get('https://www.google.com/recaptcha/api/siteverify?secret=' . $captchaSecretKey . '&response=' . $captchaPost);
		$captchaResponseArray = json_decode($captchaResponse['body'], true);
		
		$jsonArray = array();
		
		if ($captchaResponseArray['success'] != true) {
			$jsonArray['response'] = 'error';
			$jsonArray['message'] = __('You appear to be a robot [captcha].', 'vnmContact');
			wp_send_json($jsonArray);
		}
	}
	
	//	Has a valid nonce been set?
	
	//	check_ajax_referer('name-of-nonce-action', '$_REQUEST-var-name')
	
	if (!isset($_POST['nonce']) || empty($_POST['nonce']) || !check_ajax_referer('contact-nonce', 'nonce')) {
		$jsonArray['response'] = 'error';
		$jsonArray['reason'] = __('Could not verify submission', 'vnmContact');
		
		wp_send_json($jsonArray);
	}
	
	//	Has a honeypot value been sent?
	
	if (!empty($_POST['fax'])) {
		$jsonArray['response'] = 'error';
		$jsonArray['reason'] = __('You appear to be a robot [other].', 'vnmContact');
		
		wp_send_json($jsonArray);
	}
	
	//	Get unique value (in case there's more than one form on the page)
	
	$uniqueID = sanitize_title($_POST['uniqueid']);
	
	//	Grab the appropriate form to determine the acceptable vars
	
	$formName = sanitize_title($_POST['formslug']);

	$formQuery = get_posts(
		array(
			'name'				=> $formName,
			'post_type'			=> 'vnmcontactform',
			'posts_per_page'	=> 1,
		)
	);
	
	if (!$formQuery) {
		$jsonArray['response'] = 'error';
		$jsonArray['reason'] = __('Appropriate form not found. Please contact support.', 'vnmContact');
		
		wp_send_json($jsonArray);
	}
	
	//	We've gotten this far - let's get the form type!
	
	$formID = $formQuery[0]->ID;
	
	$formType = get_field('form-type', $formID);
	
	$formGroupArray = get_field('standard-form', $formID);
	
	//	We've gotten this far - let's get those fields and generate some acceptable $_POST vars
	
	$formFieldsArray = $formGroupArray['fields'];
	$acceptedVarsArray = array();
	
	$primaryEmailSlug = '';
	
	foreach ($formFieldsArray as $fieldArray) {
		$slug = sanitize_title($fieldArray['label']);	//	This is exactly what's done when generating the form field, so it should be identical
		$name = sanitize_text_field($fieldArray['label']);
		
		//	Is this the primary email field?
		
		if ($fieldArray['field-validation'] == 'email' && $fieldArray['primary-email']) {
			$primaryEmailSlug = $slug;
		}
		
		$acceptedVarsArray[$slug] = $name;
	}
	
	//	Get the POST vars
	
	$sanitizedVarsArray = array();
	
	foreach ($acceptedVarsArray as $acceptableVarKey=>$acceptableVarTitle) {
		$sanitizedVarsArray[$acceptableVarKey] = sanitize_text_field($_POST[$acceptableVarKey]);
		
		//	Check email validity
		
		if ($acceptableVarKey == 'email' || stripos($acceptableVarKey, 'email') !== false) {
			$email = $_POST[$acceptableVarKey];
			$originalEmail = $email;
			
			$email = filter_var($email, FILTER_SANITIZE_EMAIL);
			
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$jsonArray['response'] = 'error';
				$jsonArray['message'] = sprintf(_x('%s would not validate.', 'Email would not validate', 'vnmContact'), $originalEmail);
				wp_send_json($jsonArray);
			}
			
			//	Otherwise fine - let's sanitize it!
			
			$sanitizedVarsArray[$acceptableVarKey] = sanitize_email($_POST[$acceptableVarKey]);
		}
	}
	
	//	Set thanks message
	
	$thanksMessage = $contactFormArray['form-response'];
	
	//	Set email and subject
	
	$sendEmail = $contactFormArray['recipients'];
	
	if (!$sendEmail || $sendEmail == '') {
		$sendEmail = 'lawrie@wepixel.co.uk';
	}

	$subject = (isset($_POST['subject'])) ? sanitize_text_field($_POST['subject']) : $contactFormArray['subject'];

	$subject = apply_filters('vnm_contact_email_subject', $subject, $formFieldsArray);
	
	$message = '';
	
	//	Perform any additional actions
	
	do_action('vnm_contact_before_send', $sanitizedVarsArray);
	
	//	Do we ACTUALLY want to send an email?
	
	$actuallySendEmail = apply_filters('vnm_contact_actually_send_email', true);
	
	if ($actuallySendEmail) {
		foreach ($sanitizedVarsArray as $sanitizedKey=>$submittedValue) {
			//$emailArray[$acceptedVarsArray[$sanitizedKey]] = $submittedValue;
			$message .= $acceptedVarsArray[$sanitizedKey] . ': ' . $submittedValue . "\r\n";
		}
		
		$headers .= 'Reply-To: <' . $sanitizedVarsArray[$primaryEmailSlug] . '>' . "\r\n";
		$headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		
		error_log($headers . "\n\n" . $message . "\n\n", 3, dirname(__FILE__) . '/email_log.log');
		
		$sendResult = @wp_mail($sendEmail, $subject, $message, $headers);
		
		//	Return with a successful JSON response
		
		$jsonArray['email'] = $message;
		
		$jsonArray['response'] = 'success';
		$jsonArray['result'] = $sendResult;
		$jsonArray['uniqueid'] = $uniqueID;
		$jsonArray['message'] = $thanksMessage;
		
		wp_send_json($jsonArray);
	}
}

add_action('wp_ajax_vnmContactSend', 'vnmContact_checkSend');
add_action('wp_ajax_nopriv_vnmContactSend', 'vnmContact_checkSend');

?>