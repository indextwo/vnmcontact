<?php

///
//	VNM Contact - Hooks to deal with the various form functions
///

///
//	If the cookie has been set, show the 'continue' button instead
//	$formID: (int): Numeric ID of of the Contact Form post
//	$attributes: (array): Array of shortcode attributes
///

function vnmContact_cookieSetContinue($formID, $attributes, $return = false) {

	$successRedirect = $attributes['success'];
	$download = $attributes['download'];

	$successLink = get_permalink($successRedirect);

	//	Is there a download rather than a success redirect?
	
	if (!$successRedirect) {
		if (is_numeric($download)) {
			$successLink = do_shortcode('[download_data id="' . $downloadID . '" data="download_link"]');
		} else {
			$successLink = $download;	//	It's not numeric, so surely it must be a URL?
		}
	}

	$cookieMessage = get_field('cookie-message', $formID);
	$cookieMessage = str_ireplace('[successlink]', '<a href="' . $successLink . '">', $cookieMessage);
	$cookieMessage = str_ireplace('[/successlink]', '</a>', $cookieMessage);

	$cookieButton = get_field('cookie-button', $formID);
	
	if (isset($_COOKIE['leadgen_complete']) && $_COOKIE['leadgen_complete'] == 'completed') : ?>
		
		<div class="leadgen-complete">
			<?php
				echo $cookieMessage;
			?>

			<?php if ($cookieButton) : ?>
				<a href="<?php echo $successLink; ?>" class="wp-block-button__link"><?php echo sanitize_text_field($cookieButton); ?></a>
			<?php endif; ?>
		</div>
		
	<?php endif; 
}

add_action('vnm_contact_after_form', 'vnmContact_cookieSetContinue', 10, 2);

///
//	Apply a filter to the form options object so that the 'after-form' markup is available to JS
///

function vnmContact_cookieFormOptionsFilter($optionsArray) {
	return $optionsArray;
}

add_filter('vnm_form_options', 'vnmContact_cookieFormOptionsFilter', 10, 1);

?>