<?php

///
//	Returning form fields to the form builder
//	This is only for 'standard' forms - integrations like Marketo, MailChimp etc. don't make use of it
///

function vnmContact_returnField($type, $uniqueID, $array) {
	
	$fieldRequiredClass = ($array['required'] == 1) ? ' required' : '';
	$fieldName = ($array['field-key'] != '') ? $array['field-key'] : sanitize_title($array['label']);
	$uniqueFieldName = sanitize_title($array['label']) . '-' . $uniqueID;
	$fieldWidth = $array['width'];
	$cssClasses = sanitize_text_field($array['css-classes']);
	
	$fieldMessage = (!$array['validation-message']) ? '' : ' data-message="' . sanitize_text_field($array['validation-message']) . '"';
	$validationAttr = (!$array['field-validation']) ? '' : ' data-type="' . $array['field-validation'] . '"';
	
	$checkboxLabel = (!$array['checkbox-label']) ? '' : $array['checkbox-label'];
	
	$fieldWrapperClasses = 'form-row formfield box ' . $fieldWidth . ' ' . $fieldName . $fieldRequiredClass;
	$fieldWrapperClasses = apply_filters('vnm_contact_field_wrapper_classes', $fieldWrapperClasses, $array);
	
	$divOpener = '<div class="' . $fieldWrapperClasses . '">';
	
	$requiredString = '';
	
	if ($array['required']) {
		$requiredString = '<abbr class="spotcolor bold required" title="required">*</abbr>';
	}
	
	$labelString = '';
	
	if ($array['show-label']) {
		$labelString = '<label class="block" for="' . $uniqueFieldName . '">';
		$labelString .= $array['label'];
		
		if ($array['required']) {
			$labelString .= $requiredString;
		}
		
		$labelString .= '</label>';
	}
	
	$inputString = '';
	
	$filterArray = array(
		'fieldarray'		=> $array,
		'type'				=> $type,
		'uniqueid'			=> $uniqueID,
		'uniquename'		=> $uniqueFieldName,
		'fieldname'			=> $fieldName,
		'cssclasses'		=> $cssClasses,
		'requiredclass'		=> $fieldRequiredClass,
		'validationattr'	=> $validationAttr,
		'validationmessage'	=> $fieldMessage,
	);
	
	switch ($type) {
		case 'text' : {
			
			$basicFieldType = 'text';
			
			if ($array['field-validation'] == 'num') {
				$basicFieldType = 'tel';
			}
			
			$fieldClasses = apply_filters('vnm_contact_field_text_classes', 'input-text inputfield ' . $cssClasses . $fieldRequiredClass, $array);
			
			$inputString .= '<input id="' . $uniqueFieldName . '" data-id="' . $fieldName . '" name="' . $fieldName . '" class="' . $fieldClasses . '" ' . $validationAttr . ' type="' . $basicFieldType . '" ' . $fieldMessage . ' />';
			
			break;
		}
		
		case 'select' : {
			
			$optionsArray = preg_split("/\r\n|\n|\r/", $array['options']);
			
			$fieldClasses = apply_filters('vnm_contact_field_select_classes', 'inputfield ' . $cssClasses . $fieldRequiredClass, $array);
			
			$inputString .= '<div class="customdropdown">';
			$inputString .= '<select id="' . $uniqueFieldName . '" data-id="' . $fieldName . '" name="' . $fieldName . '" class="' . $fieldClasses . '"' . $fieldMessage . '>';
			$inputString .= '<option value="">' . $array['empty-option'] . '</option>';
			
			foreach ($optionsArray as $option) {
				$key = (strpos($option, ' : ') !== false) ? explode(' : ', $option)[0] : sanitize_title($option);
				$value = (strpos($option, ' : ') !== false) ? explode(' : ', $option)[1] : sanitize_text_field($option);
				
				$inputString .= '<option value="' . $key . '">' . $value . '</option>';
			}
			
			$inputString .= '</select>';
			$inputString .= '</div>';
			
			break;
		}
		
		case 'textarea': {
			
			$fieldClasses = apply_filters('vnm_contact_field_textarea_classes', 'input-text inputfield ' . $cssClasses . $fieldRequiredClass, $array);
			
			$inputString .= '<textarea id="' . $uniqueFieldName . '" data-id="' . $fieldName . '" name="' . $fieldName . '" class="' . $fieldClasses . '" rows="8"' . $fieldMessage . '></textarea>';
			
			break;
		}
		
		case 'datepicker': {
			
			$fieldClasses = apply_filters('vnm_contact_field_datepicker_classes', 'input-text inputfield ' . $cssClasses . $fieldRequiredClass, $array);
			
			$inputString .= '<input id="' . $uniqueFieldName . '" data-id="' . $fieldName . '" name="' . $fieldName . '" type="text" class="' . $fieldClasses . '" ' . $fieldMessage . ' data-field="date" />';
			
			$inputString .= '<div class="datebox"></div>';	//	This may result in multiple dateboxes, but only the first one on the page is assigned the appropriate functionality in JS
			break;
		}
		
		case 'checkbox' : {
			
			$fieldClasses = apply_filters('vnm_contact_field_checkbox_classes', 'input-checkbox inputfield ' . $cssClasses . $fieldRequiredClass, $array);
			
			$inputString .= '<input id="' . $uniqueFieldName . '" data-id="' . $fieldName . '" name="' . $fieldName . '" class="' . $fieldClasses . '" type="checkbox" ' . $fieldMessage . ' />';
			$inputString .= '<label for="' . $uniqueFieldName . '" class="checkbox">' . $checkboxLabel . $requiredString . '</label>';
			
			break;
		}
		
		case 'radio' : {
			
			$optionsArray = preg_split("/\r\n|\n|\r/", $array['radio-options']);
			
			$inputString .= '<ul class="radio-options nolist nopadding" ' . $fieldMessage . '>';
			
			$fieldClasses = apply_filters('vnm_contact_field_radio_classes', 'input-radio inputfield ' . $cssClasses, $array);
			
			foreach ($optionsArray as $option) {
				
				$key = (strpos($option, ' : ') !== false) ? explode(' : ', $option)[0] : sanitize_title($option);
				$value = (strpos($option, ' : ') !== false) ? explode(' : ', $option)[1] : sanitize_text_field($option);
				
				$uniqueOption = $key . '_' . $uniqueID;
				
				$inputString .= '<input id="' . $uniqueOption . '" data-id="' . $fieldName . '" name="' . $fieldName . '" value="' . $key . '" class="' . $fieldClasses . '" type="radio" />';
				$inputString .= '<label for="' . $uniqueOption . '" class="radio-label">' . $value . '</label>';
			}
			
			$inputString .= '</ul>';
			
			break;
		}
	}
	
	//	These are in place to add filters that *could* be hooked into to extend this plugin's functionality; however, I haven't quite figured this out yet.
	
	$labelString = apply_filters('vnm_contact_field_label', $labelString, $filterArray);
	$inputString = apply_filters('vnm_contact_input_type', $inputString, $filterArray);
	
	ob_start();
	do_action('vnm_contact_after_field_' . $fieldName, $array);
	$afterField = ob_get_clean();
	
	$fieldString = $divOpener . $labelString . $inputString . $afterField . '</div>';
	
	return $fieldString;
}

?>