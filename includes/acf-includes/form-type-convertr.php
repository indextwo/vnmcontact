<?php

return array(
	'key' => 'field__acf_vnmcontact_contactforms_group_convertr',
	'label' => 'Convertr Form',
	'name' => 'convertr-form',
	'type' => 'group',
	'conditional_logic' => array(
		array(
			array(
				'field' => 'field__acf_vnmcontact_contactforms_type',
				'operator' => '==',
				'value' => 'convertr',
			),
		),
	),
	'layout' => 'block',
	'sub_fields' => array(
		array(
			'key' => 'field__acf_vnmcontact_convertr_alert',
			'label' => 'Convertr form configuration',
			'type' => 'message',
			'message' => '<h1>Important:</h1>If Convertr is being used, then the person setting up the form <em>within Convertr</em> needs to add the following code to the <strong>Thanks HTML</strong> tab, and be sure to strip out all CSS: <pre>&lt;script&gt;
	let gateFormSubmit;
	let leagenSuccess;

	if (window.CustomEvent && typeof window.CustomEvent === \'function\') {
		gateFormSubmit = new CustomEvent(\'gate_form_submitted\');
		leagenSuccess = new CustomEvent(\'leadgen_success\');
	} else {
		gateFormSubmit = document.createEvent(\'CustomEvent\');
		gateFormSubmit.initCustomEvent(\'gate_form_submitted\', true, true);

		leagenSuccess = document.createEvent(\'CustomEvent\');
		leagenSuccess.initCustomEvent(\'leadgen_success\', true, true);
	}

	document.body.dispatchEvent(leagenSuccess);
	document.body.dispatchEvent(gateFormSubmit);
&lt;/script&gt;</pre>',
			'new_lines' => '',
			'esc_html' => 0,
		),

		array(
			'key' => 'field__acf_vnmcontact_convertr_script_src',
			'label' => 'Convertr script src',
			'name' => 'convertr-script-src',
			'type' => 'text',
			'wrapper' => array(
				'width' => '50',
			),
			'instructions' => 'Source of the script, as given in the provided script\'s <code>src=""</code> attribute.'
		),
		array(
			'key' => 'field__acf_vnmcontact_convertr_script_id',
			'label' => 'Convertr Script ID',
			'name' => 'convertr-script-id',
			'type' => 'text',
			'wrapper' => array(
				'width' => '50',
			),
			'instructions' => 'Script ID must match EXACTLY with what\'s in the provided script\'s <code>id=""</code> attribute.'
		),
	),
)

?>