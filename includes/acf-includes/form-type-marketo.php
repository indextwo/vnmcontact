<?php

return array(
	'key' => 'field__acf_vnmcontact_contactforms_group_marketo',
	'label' => 'Marketo Form',
	'name' => 'marketo-form',
	'type' => 'group',
	'conditional_logic' => array(
		array(
			array(
				'field' => 'field__acf_vnmcontact_contactforms_type',
				'operator' => '==',
				'value' => 'marketo',
			),
		),
	),
	'layout' => 'block',
	'sub_fields' => array(
		array(
			'key' => 'field__acf_vnmcontact_marketo_domain',
			'label' => 'Marketo Form Domain',
			'name' => 'marketo-form-domain',
			'type' => 'text',
			'instructions' => 'Domain for the Marketo script;<br />e.g. <code>app-sjg.marketo.com</code> or <code>www2.custom-domain.net</code>.',
			'wrapper' => array(
				'width' => '30',
			),
			'placeholder' => 'app.marketo.com',
			'prepend' => '//',
			'append' => '/js/forms2/js/forms2.min.js',
		),
		array(
			'key' => 'field__acf_vnmcontact_marketo_munchkin_id',
			'label' => 'Marketo Munchkin ID',
			'name' => 'marketo-munchkin-id',
			'type' => 'text',
			'instructions' => '(First argument of the embed code; e.g. <code>718-GIV-198</code>)',
			'wrapper' => array(
				'width' => '20',
			),
			'placeholder' => '718-GIV-198',
		),
		array(
			'key' => 'field__acf_vnmcontact_marketo_form_id',
			'label' => 'Marketo Form ID',
			'name' => 'marketo-form-id',
			'type' => 'number',
			'instructions' => '(Second argument of the embed code; always a number, e.g. <code>1882</code>)',
			'wrapper' => array(
				'width' => '20',
			),
			'placeholder' => 1882,
			'min' => 0,
		),
		array(
			'key' => 'field__acf_vnmcontact_marketo_whitepaper',
			'label' => 'Marketo Whitepaper Field',
			'name' => 'marketo-whitepaper-field',
			'type' => 'text',
			'instructions' => 'The <code>name</code> of the hidden input field to prepopulate with the whitepaper name (this is optional)',
			'wrapper' => array(
				'width' => '30',
			),
			'placeholder' => 'Asset_Name__c',
		),
		array(
			'key' => 'field__acf_vnmcontact_marketo_response',
			'label' => 'Form Response',
			'name' => 'marketo-response',
			'type' => 'textarea',
			'instructions' => 'Response to be shown once the form has been completed.<br />If you are using the shortcode with a <code>success</code> or <code>download</code> parameters, you can enter a link like <code>&lt;a href="{success/download}"&gt;Click here&lt;/a&gt;</code> and the link will automatically be grabbed from the shortcode.',
			'placeholder' => 'Thanks for your info!',
		),
	),
)

?>