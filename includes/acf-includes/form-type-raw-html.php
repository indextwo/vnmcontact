<?php

return array(
	'key' => 'field__acf_vnmcontact_contactforms_group_raw_html',
	'label' => 'Raw HTML',
	'name' => 'rawhtml-form',
	'type' => 'group',
	'conditional_logic' => array(
		array(
			array(
				'field' => 'field__acf_vnmcontact_contactforms_type',
				'operator' => '==',
				'value' => 'raw-html',
			),
		),
	),
	'layout' => 'block',
	'sub_fields' => array(
		array(
			'key' => 'field__acf_vnmcontact_raw_html_html',
			'label' => 'Raw HTML markup',
			'name' => 'raw-html',
			'type' => 'textarea',
			'instructions' => 'Paste in the raw HTML markup for this form. Note that it <strong>must not</strong> include <code>&lt;html&gt;</code>, <code>&lt;head&gt;</code> or <code>&lt;body&gt;</code> tags.',
			'wrapper' => array(
				'class' => 'code',
			),
			'rows' => 12,
		),
	),
)

?>