<?php

return array(
	'key' => 'field__acf_vnmcontact_contactforms_group_hubspot',
	'label' => 'Hubspot Form',
	'name' => 'hubspot-form',
	'type' => 'group',
	'conditional_logic' => array(
		array(
			array(
				'field' => 'field__acf_vnmcontact_contactforms_type',
				'operator' => '==',
				'value' => 'hubspot',
			),
		),
	),
	'layout' => 'block',
	'sub_fields' => array(
		array(
			'key' => 'field__acf_vnmcontact_hubspot_portal_id',
			'label' => 'Hubspot Portal ID',
			'name' => 'hubspot-portal-id',
			'type' => 'text',
			'wrapper' => array(
				'width' => '33',
			),
		),
		array(
			'key' => 'field__acf_vnmcontact_hubspot_form_id',
			'label' => 'Hubspot Form ID',
			'name' => 'hubspot-form-id',
			'type' => 'text',
			'wrapper' => array(
				'width' => '33',
			),
		),
		array(
			'key' => 'field__acf_vnmcontact_hubspot_campaign_id',
			'label' => 'Hubspot Campaign ID',
			'name' => 'hubspot-campaign-id',
			'type' => 'text',
			'wrapper' => array(
				'width' => '33',
			),
		),
	),
)

?>