<?php

return array(
	'key' => 'field__acf_vnmcontact_contactforms_group_mailchimp',
	'label' => 'MailChimp Form',
	'name' => 'mailchimp-form',
	'type' => 'group',
	'conditional_logic' => array(
		array(
			array(
				'field' => 'field__acf_vnmcontact_contactforms_type',
				'operator' => '==',
				'value' => 'mailchimp',
			),
		),
	),
	'layout' => 'block',
	'sub_fields' => array(
		array(
			'key' => 'field__acf_vnmcontact_mailchimp_api_key',
			'label' => 'Mailchimp API Key',
			'name' => 'mailchimp-api-key',
			'type' => 'text',
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field__acf_vnmcontact_contactforms_type',
						'operator' => '==',
						'value' => 'mailchimp',
					),
				),
			),
			'wrapper' => array(
				'width' => '50',
			),
		),
		array(
			'key' => 'field__acf_vnmcontact_mailchimp_list_id',
			'label' => 'Mailchimp List ID',
			'name' => 'mailchimp-list-id',
			'type' => 'text',
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field__acf_vnmcontact_contactforms_type',
						'operator' => '==',
						'value' => 'mailchimp',
					),
				),
			),
			'wrapper' => array(
				'width' => '50',
			),
		),
		array(
			'key' => 'field__acf_vnmcontact_mailchimp_html',
			'label' => 'MailChimp HTML markup',
			'name' => 'mailchimp-html',
			'type' => 'textarea',
			'instructions' => 'Everything between the <strong>&lt;form&gt;&lt;/form&gt;</strong> tags - do <strong>not</strong> include the <em>&lt;script&gt;</em> tags at the end of the markup.',
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field__acf_vnmcontact_contactforms_type',
						'operator' => '==',
						'value' => 'mailchimp',
					),
				),
			),
			'wrapper' => array(
				'class' => 'code',
			),
			'rows' => 12,
		),
		array(
			'key' => 'field__acf_vnmcontact_mailchimp_javascript',
			'label' => 'MailChimp JS markup',
			'name' => 'mailchimp-js',
			'type' => 'textarea',
			'instructions' => 'Everything between the last <strong>&lt;script&gt;&lt;/script&gt;</strong> tags as part of the embedded HTML - should start with <strong>window.fnames = new Array();</strong>. If you can, remove the wrapping &lt;script&gt; tags and the last line: `var $mcj = jQuery.noConflict(true);`',
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field__acf_vnmcontact_contactforms_type',
						'operator' => '==',
						'value' => 'mailchimp',
					),
				),
			),
			'wrapper' => array(
				'class' => 'code',
			),
			'rows' => 12,
		),
	),
)

?>