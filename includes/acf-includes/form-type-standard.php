<?php

//	Apply a filter to make the field type filterable

$initialChoicesArray = array(
	'text' => 'Text Field',
	'textarea' => 'Text Area',
	'select' => 'Dropdown',
	'checkbox' => 'Checkbox',
	'radio' => 'Radio Buttons',
	'datepicker' => 'Date Picker',
);

$fieldTypeChoicesArray = apply_filters('vnm_contact_field_types', $initialChoicesArray);

//	Initial fields (ostensibly for every field type)

$initialFieldOptionsArray = array(
	array(
		'key' => 'field__acf_vnmcontact_contactforms_field_label',
		'label' => 'Label',
		'name' => 'label',
		'type' => 'text',
		'instructions' => 'Field name/label',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '30',
		),
		'placeholder' => 'Name',
	),
	
	array(
		'key' => 'field__acf_vnmcontact_contactforms_field_show_label',
		'label' => 'Show label?',
		'name' => 'show-label',
		'type' => 'true_false',
		'instructions' => 'Label is displayed by default.',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '20',
		),
		'default_value' => 1,
		'ui' => 1,
	),
	
	array(
		'key' => 'field__acf_vnmcontact_contactforms_field_type',
		'label' => 'Field Type',
		'name' => 'field-type',
		'type' => 'select',
		'instructions' => 'Type of field',
		'required' => 0,
		'wrapper' => array(
			'width' => '30',
		),
		'choices' => $fieldTypeChoicesArray,
		'default_value' => array(
			0 => 'text',
		),
		'allow_null' => 0,
		'multiple' => 0,
		'ui' => 0,
		'ajax' => 0,
		'return_format' => 'value',
		'placeholder' => '',
	),
	
	array(
		'key' => 'field__acf_vnmcontact_contactforms_field_required',
		'label' => 'Required?',
		'name' => 'required',
		'type' => 'true_false',
		'instructions' => 'Is this field required?',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '20',
		),
		'default_value' => 0,
		'ui' => 1,
	),
	
	array(
		'key' => 'field__acf_vnmcontact_contactforms_field_key',
		'label' => 'Field Name/Key',
		'name' => 'field-key',
		'type' => 'text',
		'instructions' => 'By default a sanitized version of the label is used; if you want to use a specific field name, enter it here.',
		'placeholder' => 'my_field',
	),
);

$initialFieldOptionsArray = apply_filters('vnm_contact_initial_fields', $initialFieldOptionsArray);

///
//	Conditional field options
///

$conditionalFieldsArray = array(
	
	///
	//	<select> field options, including an empty 'please select' option
	///
	
	'select_options_empty' => array(
		'key' => 'field__acf_vnmcontact_contactforms_field_option_empty',
		'label' => '`Please Select` option',
		'name' => 'empty-option',
		'type' => 'text',
		'instructions' => 'Initial \'empty \'option prompting the user to make a selection.',
		'required' => 0,
		'default_value' => 'Please choose',
		'conditional_logic' => array(
			array(
				array(
					'field' => 'field__acf_vnmcontact_contactforms_field_type',
					'operator' => '==',
					'value' => 'select',
				),
			),
		),
		'wrapper' => array(
			'width' => '50',
		),
	),
	
	'select_options' => array(
		'key' => 'field__acf_vnmcontact_contactforms_field_options',
		'label' => 'Options',
		'name' => 'options',
		'type' => 'textarea',
		'instructions' => 'Add each option on a new line. Separate key/values with <code> : </code> (<code>space-colon-space</code>)',
		'required' => 0,
		'conditional_logic' => array(
			array(
				array(
					'field' => 'field__acf_vnmcontact_contactforms_field_type',
					'operator' => '==',
					'value' => 'select',
				),
			),
		),
		'wrapper' => array(
			'width' => '50',
		),
		'new_lines' => '',
	),
	
	///
	//	Checkbox label
	///
	
	'checkbox_label' => array(
		'key' => 'field__acf_vnmcontact_contactforms_field_checkbox_label',
		'label' => 'Checkbox label description',
		'name' => 'checkbox-label',
		'type' => 'text',
		'instructions' => 'A sentence to describe this checkbox option.',
		'required' => 0,
		'placeholder' => 'I accept the terms and conditions',
		'conditional_logic' => array(
			array(
				array(
					'field' => 'field__acf_vnmcontact_contactforms_field_type',
					'operator' => '==',
					'value' => 'checkbox',
				),
			),
		),
	),
	
	///
	//	Radio-button field options
	///
	
	'radio_options' => array(
		'key' => 'field__acf_vnmcontact_contactforms_field_radio_options',
		'label' => 'Radio button option',
		'name' => 'radio-options',
		'type' => 'textarea',
		'instructions' => 'Add each option on a new line. Separate key/values with <code> : </code>',
		'required' => 0,
		'conditional_logic' => array(
			array(
				array(
					'field' => 'field__acf_vnmcontact_contactforms_field_type',
					'operator' => '==',
					'value' => 'radio',
				),
			),
		),
		'new_lines' => '',
	),
);

$conditionalFieldsArray = apply_filters('vnm_contact_conditional_fields', $conditionalFieldsArray);

///
//	Closing fields (note that this does contain a conditional field for EMAIL type, but that really should not be screwed with)
///

$closingFieldsArray = array(
	array(
		'key' => 'field__acf_vnmcontact_contactforms_field_width',
		'label' => 'Field Width',
		'name' => 'width',
		'type' => 'select',
		'instructions' => 'Select the width of the field (if using the VNM Responsiflex framework)',
		'required' => 0,
		'conditional_logic' => 0,
		'choices' => array(
			'six' => 'Half Width',
			'twelve' => 'Full Width',
		),
		'default_value' => array(
			0 => 'six',
		),
		'wrapper' => array(
			'width' => '50',
		),
		'allow_null' => 0,
		'multiple' => 0,
		'ui' => 0,
		'ajax' => 0,
		'return_format' => 'value',
	),
	
	array(
		'key' => 'field__acf_vnmcontact_contactforms_field_classes',
		'label' => 'Custom CSS Classes',
		'name' => 'css-classes',
		'type' => 'text',
		'instructions' => 'Additional CSS classes for this field',
		'wrapper' => array(
			'width' => '50',
		),
	),
	
	array(
		'key' => 'field__acf_vnmcontact_contactforms_field_validation',
		'label' => 'Field Validation',
		'name' => 'field-validation',
		'type' => 'select',
		'instructions' => 'Does this field require special validation (i.e. an email or a number)?',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '50',
		),
		'choices' => array(
			'none' => 'Regular',
			'email' => 'Email',
			'num' => 'Number',
		),
		'default_value' => array(
			0 => 'none',
		),
		'allow_null' => 0,
		'multiple' => 0,
		'ui' => 0,
		'ajax' => 0,
		'return_format' => 'value',
	),
	
	array(
		'key' => 'field__acf_vnmcontact_contactforms_field_validation_message',
		'label' => 'Validation Message',
		'name' => 'validation-message',
		'type' => 'text',
		'instructions' => 'Message if there\'s a problem with this field. If left blank, will show default.',
		'required' => 0,
		'conditional_logic' => 0,
		'wrapper' => array(
			'width' => '50',
		),
		'placeholder' => 'Please enter a valid email',
	),
	
	//	Is this the primary email field?
	
	array(
		'key' => 'field__acf_vnmcontact_contactforms_field_primary_email',
		'label' => 'Is this the primary email field?',
		'name' => 'primary-email',
		'type' => 'true_false',
		'required' => 0,
		'conditional_logic' => array(
			array(
				array(
					'field' => 'field__acf_vnmcontact_contactforms_field_validation',
					'operator' => '==',
					'value' => 'email',
				),
			),
		),
		'ui' => 1,
	),
);

///
//	Create an array of arrays to loop through.
///

$acfContactFieldsArray = array(
	'init'			=> $initialFieldOptionsArray,
	'conditional'	=> $conditionalFieldsArray,
	'post'			=> $closingFieldsArray,
);

//	Apply a filter to add other conditional fields to ACF

$acfContactFieldsArray = apply_filters('vnm_contact_acf_fields_filter', $acfContactFieldsArray);

///
//	Now loop through the collection and 'flatten' any arrays 
///

$acfFieldsArray = array();

foreach ($acfContactFieldsArray as $arrayCollection) {
	if (!empty($arrayCollection)) {
		foreach ($arrayCollection as $array) {
			$acfFieldsArray[] = $array;
		}
	}
}

///
//	Now return the actual ACF group
///

return array(
	'key' => 'field__acf_vnmcontact_contactforms_group_standard',
	'label' => 'Standard Form',
	'name' => 'standard-form',
	'type' => 'group',
	'conditional_logic' => array(
		array(
			array(
				'field' => 'field__acf_vnmcontact_contactforms_type',
				'operator' => '==',
				'value' => 'standard',
			),
		),
	),
	'layout' => 'block',
	'sub_fields' => array(
		
		array(
			'key' => 'field__acf_vnmcontact_contactforms_subject',
			'label' => 'Email Subject',
			'name' => 'subject',
			'type' => 'text',
			'instructions' => 'The email subject line.',
			'required' => 1,
			'placeholder' => 'Website Enquiry',
		),
		
		array(
			'key' => 'field__acf_vnmcontact_contactforms_intro_message',
			'label' => 'Email Intro Message',
			'name' => 'intro-message',
			'type' => 'textarea',
			'instructions' => 'Add a message to the start of the email. Note that this is for internal use only.',
			'required' => 0,
			'new_lines' => '',
			'rows' => 3,
		),
		
		array(
			'key' => 'field__acf_vnmcontact_contactforms_fields_repeater',
			'label' => 'Fields',
			'name' => 'fields',
			'type' => 'repeater',
			'required' => 0,
			'collapsed' => 'field__acf_vnmcontact_contactforms_field_label',
			'min' => 0,
			'max' => 0,
			'layout' => 'block',
			'button_label' => 'Add Field',
			'sub_fields' => $acfFieldsArray,
		),
		array(
			'key' => 'field__acf_vnmcontact_contactforms_form_captcha',
			'label' => 'Include Captcha?',
			'name' => 'has-captcha',
			'type' => 'true_false',
			'instructions' => 'If enabled, will include Google Captcha for additional form validation',
			'required' => 0,
			'default_value' => 0,
			'ui' => 1,
		),
		array(
			'key' => 'field__acf_vnmcontact_contactforms_form_recipients',
			'label' => 'Email Recipients',
			'name' => 'recipients',
			'type' => 'text',
			'instructions' => 'Who will receive emails from this contact form. For multiple recipients, separate addresses with a comma; e.g. <code>bill@email,com,jill@email.com</code>.',
			'required' => 1,
		),
		
		array(
			'key' => 'field__acf_vnmcontact_contactforms_field_ajax_post',
			'label' => 'Use standard AJAX form post?',
			'name' => 'ajax-post',
			'type' => 'true_false',
			'required' => 0,
			'ui' => 1,
			'default_value' => 1,
		),
		
		//	AJAX thank you message
		
		array(
			'key' => 'field__acf_vnmcontact_contactforms_form_response',
			'label' => 'Form Response',
			'name' => 'form-response',
			'type' => 'text',
			'instructions' => 'This is the thank you message displayed after a user has successfully submitted this contact form.',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field__acf_vnmcontact_contactforms_field_ajax_post',
						'operator' => '==',
						'value' => 1,
					),
				),
			),
		),
		
		//	Standard POST action
		
		array(
			'key' => 'field__acf_vnmcontact_contactforms_post_action',
			'label' => 'POST action',
			'name' => 'post-action',
			'type' => 'text',
			'instructions' => 'You will need to create action hooks for <code>admin_post_{$action}</code> &amp; <code>admin_post_nopriv_{$action}</code> that will handle form submission &amp; redirection. More info is in the README file.',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field__acf_vnmcontact_contactforms_field_ajax_post',
						'operator' => '==',
						'value' => 0,
					),
				),
			),
		),
		
	),
)

?>