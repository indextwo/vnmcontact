<?php

///
//	VNM Contact - Parse incoming requests
///

///
//	Sniff the incoming requests. If the URL contains `gatecomplete`, then set the `leadgen_complete` cookie
///

function vnmContact_parseRequests() {
	if (isset($_GET['gatecomplete'])) {
		$cookiePath = (get_field('vnmcontact-cookie-is-single-page', 'option')) ? '' : '/';
		
		setcookie('leadgen_complete', 'completed', time() + 7776000, $cookiePath);	//	90 days
	}
}

add_action('parse_request', 'vnmContact_parseRequests', 10);

?>