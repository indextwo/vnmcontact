<?php

///
//	Lock Bar Block
///

$formLink   = get_field('link');

$content_styles  = '';
$style_attribute = '';
$formatting = get_field('formatting');

if (isset($formatting['background_color']) && !empty($formatting['background_color'])) {
	$content_styles .= 'background-color: ' . $formatting['background_color'] . ';';
}

if (isset($formatting['text_color']) && !empty($formatting['text_color'])) {
	$content_styles .= 'color: ' . $formatting['text_color'] . ';';
}

if (isset($formatting['top_margin']) && !empty($formatting['top_margin'])) {
	$content_styles .= 'margin-top: ' . $formatting['top_margin'] . 'px;';
}

if (!empty($content_styles)) {
	$style_attribute = ' style="' . $content_styles . '"';
}

?>

<div class="wp-block-custom lock-content-bar">
	<div class="lock-content-banner primary--bg primary--text"<?php echo wp_kses( $style_attribute, array( 'style' => true ) ); ?>>
		<div class="container">
			<div class="row">

				<div class="lock-content-banner__col">
					<?php if (get_field('title')) : ?>
						<h2 class="lock-content-banner__title"><?php the_field('title'); ?></h2>
					<?php endif; ?>		

					<div class="lock-content-banner__content">
						<?php the_field('content'); ?>
					</div>

					<?php if ($formLink) : ?>
						<div class="lock-content-banner__button">
							<a class="btn" href="<?php echo esc_url($formLink['url']); ?>" target="_self">
								<?php echo esc_html($formLink['title']); ?>
							</a>
						</div>
					<?php endif; ?>
				</div>

			</div>
		</div>
	</div>
</div>
