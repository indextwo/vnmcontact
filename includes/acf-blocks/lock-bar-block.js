jQuery(function($) {

	//#########################################################################################################################################//
	//	
	//	OKAY! So, the way this works is that ALL content is initially visible; but if a lock-bar is detected, it will then HIDE all of the
	//	content after that point. All this means is that if the leadgen_completed cookie IS found, the content STAYS visible - it doesn't get
	//	hidden by the following condition. So the same now goes for the nav-bar!
	//	
	//########################################################################################################################################//

	//	Let's hide all of the content up until the unlock point

	if (!$('.lock-content-bar').length) {
		console.log('Unable to find a .lock-content-bar element, so I`m not trying to block anything.');
		return;
	}

	if (Cookies.get('leadgen_complete') && Cookies.get('leadgen_complete') == 'completed') {
		$('.lock-content-bar').addClass('completed');
	} else {
		console.log('Content is locked, because form-complete cookie no exist');
		// var contentTop = $('.content-area').offset().top;
		// var lockbarTop = $('.lock-content-bar').first().offset().top + $('.lock-content-bar').first().outerHeight();
		// var lockedHeight = lockbarTop - contentTop;
		
		// $('.content-area').height(lockedHeight);

		$('.lock-content-bar').first().nextAll().addClass('hide');
		$('nav.nav-bar').addClass('hide');
		$('.content-area').addClass('locked');
	}

	///
	//	If the content is locked and needs unlocking:
	///

	$(document.body).on('maybe_unlock_content', function() {
		var unlockableContent = false;

		if (typeof formsObject.unlock != 'undefined') {
			if (formsObject.unlock != false) {

				unlockableContent = true;

				$('.lock-content-bar').slideUp();
				$('.content-area').css('height', '');
				$('.content-area').removeClass('locked');
				$('.lock-content-bar').first().nextAll().removeClass('hide');
				$('nav.nav-bar').removeClass('hide');
			}
		}
		
		//	Maybe show a response regardless
		
		var responseText = getFormattedResponse();
		
		if (responseText != '') {
			$('.vnm-contact-form-response').html(responseText);
		}

		//	If there's an unlock button, let's add that to the response; but ONLY if it's in a lightbox

		if (unlockableContent) {
			if ($('.vnm-contact-form-response').closest('.lightbox').length > 0) {
				if (typeof formsObject.unlockedbutton != 'undefined') {
					if (formsObject.unlockedbutton != '') {
						$('.vnm-contact-form-response').append('<div class="aligncenter"><label class="btn close-form">' + formsObject.unlockedbutton + '</label></div>');
					}
				}
			}
		}
	});

});