<?php

$formID = get_field('idc-gate-form');
$formSlug = get_post_field('post_name', $formID);

$unlockContent = get_field('unlock-content');
$unlockResponse = get_field('unlock-response');
$unlockedButton = get_field('unlocked-button');

$downloadMethod = get_field('download-method');

$downloadURL = get_field('download-url');
$downloadFile = get_field('download-success-file');
$downloadID = get_field('download-success-post');
$downloadResponse = get_field('download-response');
$isLightbox = get_field('form-lightbox');
$uniqueID = get_field('unique-id');
$formTitle = get_field('form-title');

$fileURL = '';

//	Which download method has actually been selected?

if ($downloadMethod == 'url') {
	$fileURL = $downloadURL;
} else if ($downloadMethod == 'upload') {
	$fileURL = $downloadFile;
} else if ($downloadMethod == 'advanced') {
	try {
		$download = download_monitor()->service('download_repository')->retrieve_single($downloadID);
		$file = $download->get_version();
		$fileURL = $file->get_url();
	} catch ( Exception $exception ) {
		//	No download found
	}
}

$whitepaperName = pathinfo($fileURL, PATHINFO_FILENAME);

$shortcodeArgs = 'id="' . esc_attr($formSlug) . '"';

if ($unlockContent) {
	$shortcodeArgs .= ' unlock=1';
} else {
	$shortcodeArgs .= ' download="' . esc_attr($fileURL) . '"';
}

//	Is there a custom whitepaper value?

if ($whitepaperName) {
	$shortcodeArgs .= ' whitepaper="' . esc_attr($whitepaperName) . '"';
}

//	Is there a custom response?

if ($unlockContent) {
	if ($unlockResponse) {
		$shortcodeArgs .= ' response="' . esc_attr($unlockResponse) . '"';
	}

	if ($unlockedButton) {
		$shortcodeArgs .= ' unlockedbutton="' . esc_attr($unlockedButton) . '"';
	}
} else {
	if ($downloadResponse) {
		$shortcodeArgs .= ' response="' . esc_attr($downloadResponse) . '"';
	}
}

//	Is it in a lightbox?
	
if ($isLightbox) {
	$shortcodeArgs .= ' lightbox=1 uniqueid="' . esc_attr($uniqueID) . '"';
}

//	Does it have a title?

if ($formTitle) {
	$shortcodeArgs .= ' title="' . esc_attr($formTitle) . '"';
}

echo do_shortcode('[wpform ' . $shortcodeArgs . ']');

?>