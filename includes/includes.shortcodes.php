<?php

///
//	VNM Contact - Add shortcodes for the contact form
///

///
//	Form shortcode
///

function vnmContact_Shortcode($atts, $content = null) {
	
	$attributes = shortcode_atts(array(
        'id' => '',
        'success' => 0,
        'download' => 0,
        'unlock' => false,
        'unlockedbutton' => '',
		'response' => '',
		'whitepaper' => '',
		'lightbox' => false,
		'uniqueid' => '',
		'title' => '',
    ), $atts);
	
	//	Get all of the contact forms and loop through them to find the appropriate one
	
	$formName = $attributes['id'];

	if (!$formName) {
		return __('No form ID given. Please ensure you have defined a form first.', 'vnmContact');
	}
	
	$successRedirect = ($attributes['success']) ? $attributes['success'] : '';
	
	$downloadRedirect = ($attributes['download']) ? $attributes['download'] : '';
	
	$unlockContent = ($attributes['unlock'] != false) ? true : false;
	$unlockedButton = ($attributes['unlockedbutton']) ? $attributes['unlockedbutton'] : '';
	
	//	Has a different kind of response been passed in the arguments? This var will be picked up by the included template
	
	$ajaxResponse = ($attributes['response']) ? $attributes['response'] : '';

	//	Get the form
	
	$formQuery = get_posts(
		array(
			'name'				=> $formName,
			'post_type'			=> 'vnmcontactform',
			'posts_per_page'	=> 1,
		)
	);
	
	if (!$formQuery) {
		return __('No form found. Please contact support.', 'vnmContact');
	}
	
	//	We've gotten this far - let's get the form type!
	
	$formID = $formQuery[0]->ID;

	$formType = get_field('form-type', $formID);
	
	//	Set some script vars in case the templates need them
	
	$cssPath = VNMCONTACT_PATH . '/css/';
	$cssURI = VNMCONTACT_URL . '/css/';
	
	$scriptPath = VNMCONTACT_PATH . '/js/';
	$scriptURI = VNMCONTACT_URL . '/js/';

	//	Localize these form option vars regardless of whether the form is actually enqueued

	$cookiesDisabled = get_field('disable-cookie', $formID);
	$cookiePath = (get_field('vnmcontact-cookie-is-single-page', 'option')) ? '' : '/';	//	This is a true_false toggle, so if it's TRUE, then it's set to SINGLE PAGE; if it's FALSE it's set to MULTI-PAGE
	$cookieExpiry = get_field('vnmcontact-cookie-expiry', 'option');

	if (!$cookieExpiry) {
		$cookieExpiry = 90;
	}

	wp_localize_script('vnm-form-globals', 'formOptionsObject', apply_filters('vnm_form_options', array(
		'disableCookies' => $cookiesDisabled,
		'cookiePath' => $cookiePath,
		'cookieExpiry' => $cookieExpiry,
	)));

	//$uniqueFormID = wp_generate_uuid4();
	$uniqueFormID = $attributes['uniqueid'];
	
	//	Create an array that MAY be localised to a script in one of the templates
	
	$formsObject = array(
		'baseURL'	=> get_home_url() . '/',
		'atts'		=> $attributes,
	);

	if ($successRedirect) {
		$formsObject['redirect'] = get_permalink($successRedirect);
	}

	if ($downloadRedirect) {
		$formsObject['download'] = $downloadRedirect;	//	This MAY BE just be an ID; or it may be a ful URL link. globals.js/leadgen-success.js work out which is which for the download
	}

	//	Is this form for unlocking content?

	$formsObject['unlock'] = $unlockContent;

	if ($unlockedButton) {
		$formsObject['unlockedbutton'] = sanitize_text_field($unlockedButton);
	}

	//	Different kind of response?

	if ($ajaxResponse) {
		$formsObject['response'] = $ajaxResponse;
	}
	
	//	Is this form for a particular whitepaper?
	
	if ($attributes['whitepaper']) {
		$formsObject['whitepaper'] = $attributes['whitepaper'];
	}
	
	//	Get started with the output
	
	ob_start();

	///
	//	Should this be in a lightbox? Then let's open a lightbox wrapper
	///

	if ($attributes['lightbox']) {
		echo '<input type="checkbox" id="wp-form-trigger-' . $uniqueFormID . '" class="trigger vnmcontactform-trigger formlightboxtrigger" autocomplete="off" />
			<div class="formlightboxoverlay">
				<label for="wp-form-trigger-' . $uniqueFormID . '" class="lightboxclose trigger-no-focus">
					<span class="hide">' . __('Close', 'vnmMyContact') . '</span>
				</label>

				<div class="lightbox">
					<label for="wp-form-trigger-' . $uniqueFormID . '" class="close-cross">
						<span class="hide">' . __('Close', 'vnmMyContact') . '</span>
					</label>';

		//	If there's a title, then let's use it

		if ($attributes['title']) {
			echo '<h2 class="form-title">' . $attributes['title'] . '</h2>';
		}
	}
	
	///
	//	Is there a leadgen cookie? In which case, let's skip the form entirely and just do the after-party:
	///

	if (isset($_COOKIE['leadgen_complete']) && $_COOKIE['leadgen_complete'] == 'completed') {
		do_action('vnm_contact_after_form', $formID, $attributes);

		//	Close the lightbox, if it's open

		if ($attributes['lightbox']) {
			echo '</div></div>';
		}

		$returnString = ob_get_clean();
		
		//	Now let's finally enqueue the CSS & validation JS
		
		// wp_enqueue_style('vnm-contact');
		// wp_enqueue_style('vnm-form-styles');

		return $returnString;
	}

	//	Otherwise, set up the form container

	?>
	
	<div class="vnm-contact-form-wrapper">
		<?php
			
			do_action('vnm_contact_form_intro_outro', $formID, 'intro');
			
			//	Get the template
			
			include(VNMCONTACT_PATH . '/templates/' . $formType . '.php');
			
			do_action('vnm_contact_form_intro_outro', $formID, 'outro');
		?>
	</div>
	
	<div class="vnm-contact-form-response"></div>

	<?php
	
	do_action('vnm_contact_after_form', $formID, $attributes);

	//	Close the lightbox, if it's open

	if ($attributes['lightbox']) {
		echo '</div></div>';
	}

	$returnString = ob_get_clean();
	
	//	Now let's finally enqueue the CSS & validation JS
	
	// wp_enqueue_style('vnm-contact');
	// wp_enqueue_style('vnm-form-styles');
	
	wp_enqueue_script('vnm-form-validation');
	
	do_action('vnm_contact_validation_scripts', $formID, $attributes);
	
	return $returnString;
}

add_shortcode('wpform', 'vnmContact_Shortcode');

///
//	Output the form intro/outro
///

function vnmContact_shortcode_formIntroOutro($postID, $type) {
	if (get_field('form-' . $type, $postID)) {
		echo '<div class="vnm-contact-form-' . $type . '">';
		the_field('form-' . $type, $postID);
		echo '</div>';
	}
}

add_action('vnm_contact_form_intro_outro', 'vnmContact_shortcode_formIntroOutro', 10, 2);

?>