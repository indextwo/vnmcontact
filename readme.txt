=== VNM Contact ===
Contributors: indextwo
Tags: contact, marketo, hubspot, mailchimp
Requires at least: 4.6
Tested up to: 5.9.2
Stable tag: 1.2.18
Requires PHP: 7.0.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

This is the VNM Contact plugin specifically built for IDC projects.

== Description ==

This is the official contact form/gate plugin for IDC projects. It must be used in conjunction with ACF Pro and Download Monitor in order to work fully. 

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Create new Contact Forms & Gates via the new Contact Forms post type
4. Follow instructions to add & configure contact forms as needed

== Changelog ==

= 1.2.18 - 2022-07-26 =

Fix: Ensure name of whitepaper is passed from Gate block to the appropriate form

= 1.2.17 - 2022-03-31 =

Make update to header download button update 'live' if there's a secondary link

= 1.2.16 - 2022-03-31 =

Update to handle cookie sessions for new 'secondary link' feature in Products

= 1.2.15 - 2022-03-11 =

Accidentally left in a debug alert :/

= 1.2.14 - 2022-03-10 =

Minor update to ensure ?gatecomplete=1 argument works on static published Products as well via JS

= 1.2.13 - 2022-03-10 =

Minor update to ensure ?gatecomplete=1 argument is setting cookie before gate is rendered

= 1.2.12 - 2022-02-08 =

Fatal error

= 1.2.11 - 2022-02-08 =

Removed API key field from settings (no longer needed)

= 1.2.10 - 2022-02-08 =

Updates to BitBucket plugin integration

= 1.2.9 - 2021-12-07 =
Bug Fix: Issue with `download` variable which could be either an ID or a URL when re-downloading after Gate had already been completed

= 1.2.8 - 2021-10-15 =
Updates to the Lock-Bar block functionality

= 1.2.7 - 2021-10-13 =
Updated even more CSS for Convertr

= 1.2.6 - 2021-10-12 =
Updated some more CSS for Convertr

= 1.2.5 - 2021-09-30 =
Updated some CSS for Convertr

= 1.2.4 - 2021-09-30 =
Updated to work with Convertr, including for iViews & CMS

= 1.2.3 - 2021-09-13 =
Added response text that seems to have disappeared from form response; forced successful download in a new tab

= 1.2.2 - 2021-09-10 =
Updated the Marketo form type to require a fully-qualified domain

= 1.2.1 - 2021-06-02 =
Updates to the paywall/lock-bar functionality

= 1.2.0 - 2021-05-27 =
Update to the plugin-update-checker add-on; changes to CSS being enqueued to ensure it works remotely

= 1.1.10 - 2021-05-21 =
Additional event triggers which can be used for GA tracking

= 1.1.9 - 2021-04-15 =
Download button for shortcode can now redirect straight to download files as well as page redirects

= 1.1.8 - 2021-02-22 =
Fixed issue with leadgen cookie not being read correctly in some circumstances

= 1.1.7 - 2021-02-11 =
Updated 'Standard Form' functionality; added `vnm_contact_email_subject` filter

= 1.1.6 - 2021-01-26 =
New 'Paywall' feature plus various other amends

= 1.1.5 - 2020-11-05 =
Updated IDC Gate Block, Hubspot auto-download handling

= 1.1.4 - 2020-10-29 =
Made 'You've already completed this form' message CMSable
Writing a cookie on completing a form is now CMSable per form

= 1.1.3 - 2020-10-20 =
Added handling for `?gatecomplete=1` URL parameter, which will automatically write the `leadgen_complete` cookie

= 1.1.2 - 2020-09-17 =
Updated to checkbox styling for Hubspot

= 1.1.1 - 2020-09-16 =
Fixed various embed issues, cookie functionality

= 1.1.0 - 2020-08-28 =
Fixed issue with Hubspot embed

= 1.0.1 - 2020-04-02 =
Removed reference to SAP-specific code (now add-ons are filterable)

= 1.0.0 - 2020-04-01 =
Initial release of VNM Contact.

== Usage ==

Add form to a page (won't do anything other than submit without additional parameters):
`[wpform id="marketo-form-checkbox"]`

Redirect to another page on success:
`[wpform id="form" success=123]`

Download document automatically on success (where `download` is Download ID):
`[wpform id="marketo-form-checkbox" download=456]`

Show a different response to the one in the form:
`[wpform id="marketo-form-checkbox" download=456 response="Thanks! Download that document <a href='{download}'>here</a>"]`

Marketo-specific: add a whitepaper name to the hidden Marketo input field:
`[wpform id="marketo-form-checkbox" download=456 whitepaper="name-of-whitepaper"]`

== Add additional form types ==

add_filter('vnm_contact_form_types', 'my_form_types', 10, 1);

function my_form_types($typesArray) {
	//	$typesArray is $key=>$readableValue where $key matches the form type
	
	return $typesArray;
}

do_action('vnm_contact_add_form_types'),	//	This needs to echo ACF field group arrays

== Add additional field types ==

add_filter('vnm_contact_field_types', 'my_field_types', 10, 1);

function my_field_types($typesArray) {
	//	$typesArray is $key=>$readableValue where $key matches
	
	return $typesArray;
}

== Output filters ==

```
do_action('vnm_contact_validation_scripts');

add_action('vnm_contact_before_form', $formID);
add_action('vnm_contact_after_form', $formID);

add_filter('vnm_contact_field_wrapper_classes', 10, 2);
add_filter('vnm_contact_field_text_classes', 10, 2);
add_filter('vnm_contact_field_select_classes', 10, 2);
add_filter('vnm_contact_field_textarea_classes', 10, 2);
add_filter('vnm_contact_field_datepicker_classes', 10, 2);
add_filter('vnm_contact_field_checkbox_classes', 10, 2);
add_filter('vnm_contact_field_radio_classes', 10, 2);

add_filter('vnm_contact_button_classes', 10, 1);

add_filter('vnm_contact_field_label', 10, 2);
add_filter('vnm_contact_input_type', 10, 2);

add_action('vnm_contact_after_field_{NAME_OF_FIELD}', 10, 1);

add_filter('vnm_contact_actually_send_email', 10, 1); 	//	Default: true

add_action('vnm_contact_before_send', 10, 1);	//	sanitized array

add_filter('vnm_contact_email_subject', 10, 2);	//	$subject text, $array of form details

add_action('vnm_contact_script_enqueue');

add_action('vnm_contact_form_action');
```