<?php
/*
Plugin Name: IDC Contact Forms
Plugin URI: http://www.verynewmedia.com
Description: Official Contact Form & Gate plugin for IDC Products & iViews
Version: 1.2.18
Author: Lawrie Malen
Author URI: http://www.verynewmedia.com
License: GPL

Copyright (C) 2013-2022 Lawrie Malen // Very New Media
Released 16-04-2013
Updated: 26-07-2022
*/

if (!defined('ABSPATH')) {
	exit;	//	Exit if accessed directly
}

///
//	Define constants
///

define('VNMCONTACT', true);
define('VNMCONTACT_VERSION', '1.2.18');
define('VNMCONTACT_FILE', __FILE__);
define('VNMCONTACT_PATH', plugin_dir_path(VNMCONTACT_FILE));
define('VNMCONTACT_URL', plugins_url('', VNMCONTACT_FILE));
define('VNMCONTACT_BASENAME', plugin_basename(VNMCONTACT_FILE));

///
//	ACTIVATION
///

function vnmContact_install() {
	$vnmContact_version = VNMCONTACT_VERSION;
	
	//	Check if ACF is active - if not, this plugin will throw a fatal error, so let's stop that
	
	if (!class_exists('ACF')) {
		echo '<h3 style="font-family: -apple-system,BlinkMacSystemFont,\'Segoe UI\',Roboto,Oxygen-Sans,Ubuntu,Cantarell,\'Helvetica Neue\',sans-serif;">' . __('Advanced Custom Fields (ACF) Pro plugin is required for VNM Contact Forms to function properly.', 'vnmContact') . '</h3>';
		
		//Adding @ before will prevent XDebug output
		
		@trigger_error(__('Please install & activate ACF Pro before activating this plugin.', 'vnmContact'), E_USER_ERROR);
	}
	
	add_option('vnmContact_version', $vnmContact_version);
}

register_activation_hook(VNMCONTACT_FILE, 'vnmContact_install');

///
//	DEACTIVATION
///

function vnmContact_deactivate() {
	delete_option('vnmContact_version');
}

register_deactivation_hook(VNMCONTACT_FILE, 'vnmContact_deactivate');

///
//	Include updates
//	https://github.com/YahnisElsts/plugin-update-checker
///

function vnmMyContact_checkUpdate() {
	require_once(VNMCONTACT_PATH . 'includes/plugin-update-checker/plugin-update-checker.php');
	
	$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
		'https://bitbucket.org/indextwo/vnmcontact',
		VNMCONTACT_FILE,
		'vnmContact',
		2
	);
}

add_action('admin_init', 'vnmMyContact_checkUpdate');

///
//	Add localisation
///

function vnmMyContact_localisation() {
	load_plugin_textdomain('vnmMyContact', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'vnmMyContact_localisation');

///
//	Determine if we need to show an alert on the Plugins page
///

function vnmContact_pluginSettingsLinks($links) {
	if (!class_exists('ACF')) {
		$acfWarning = sprintf(
			__('%sVNM Contact Forms%s needs ACF Pro to work - Install &amp; activate ACF Pro before attempting to use VNM Contact Forms.', 'vnmContact'),
			'<strong>',
			'</strong>'
		);
		
		array_push($links, $acfWarning);
	}
	
	return $links;
}

add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'vnmContact_pluginSettingsLinks');

///
//	Custom post types extension for the Contact Forms
///

function vnmContact_Register() {
	$labels = array(
		'name' => _x('Contact Forms', 'Post type name', 'vnmContact'),
		'singular_name' => _x('Contact Form', 'Post type singular name', 'vnmContact'),
		'add_new' => _x('Add New', 'New Contact Form', 'vnmContact'),
		'add_new_item' => __('Add New Contact Form', 'vnmContact'),
		'edit_item' => __('Edit Contact Form', 'vnmContact'),
		'new_item' => __('New Contact Form', 'vnmContact'),
		'view_item' => __('View Contact Form', 'vnmContact'),
		'search_items' => __('Search Contact Forms', 'vnmContact'),
		'not_found' =>  _x('Nothing found', 'No matching Forms found', 'vnmContact'),
		'not_found_in_trash' => _x('Nothing found in Trash', 'No forms found in trash', 'vnmContact'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => false,
		'publicly_queryable' => false,
		'show_ui' => true,
		'query_var' => false,
		'menu_icon' => 'dashicons-buddicons-pm',
		'rewrite' => false,
		'has_archive' => false,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array('title')
	); 

	register_post_type('vnmcontactform' , $args);
}

add_action('init', 'vnmContact_Register');

///
//	Include ACF Settings page
///

include_once('includes/includes.acf-fields.php');

///
//	Include form fields return functions (used by 'Standard'-type form)
///

include_once('includes/includes.form-fields.php');

///
//	Include URL requests
///

include_once('includes/includes.requests.php');

///
//	Include shortcodes
///

include_once('includes/includes.shortcodes.php');

///
//	Include standard form handling
///

include_once('includes/includes.form-ajax.php');

///
//	Include form hooks
///

include_once('includes/includes.hooks.php');

///
//	Include admin columns
///

include_once('includes/includes.admin-columns.php');

///
//	Add inline CSS for dashicons font
///

function vnmContact_menuIconColorCSS() {
	echo '<style>
		#menu-posts-vnmcontactform div.wp-menu-image.dashicons-buddicons-pm::before {
		  color: #3498DB;
		}

		#menu-posts-vnmcontactform .wp-menu-open div.wp-menu-image.dashicons-buddicons-pm:before {
		  color: #ECF0F1;
		}
	</style>';
}

add_action('admin_head', 'vnmContact_menuIconColorCSS');

///
//	Register validation script for enqueuing
///

function vnmContact_enqueueScripts() {
	$cssPath = VNMCONTACT_PATH . '/css/';
	$cssURI = VNMCONTACT_URL . '/css/';
	
	//	Enqueue CSS
	
	wp_enqueue_style('vnm-contact', $cssURI . 'vnmContact.css', array(), filemtime($cssPath . 'vnmContact.css'));
	wp_enqueue_style('vnm-form-styles', $cssURI . 'formstyles.css', array(), filemtime($cssPath . 'formstyles.css'));
	
	$scriptPath = VNMCONTACT_PATH . '/js/';
	$scriptURI = VNMCONTACT_URL . '/js/';

	wp_register_script('vnmcontact-captcha-script', 'https://www.google.com/recaptcha/api.js', array('jquery'), '1.0.1', true);
	
	//	Enqueue the cookie script

	wp_enqueue_script('js-cookies', $scriptURI . 'js.cookie.js', array('jquery'), filemtime($scriptPath . 'js.cookie.js'), true);

	//	Enqueue some form globals
	
	wp_enqueue_script('vnm-form-globals', $scriptURI . 'globals.js', array('js-cookies'), filemtime($scriptPath . 'globals.js'), true);

	wp_script_add_data('vnm-form-globals', 'data-test', 'This is a test value');
}

add_action('wp_enqueue_scripts', 'vnmContact_enqueueScripts');

///
//	Add a filter to add the appropriate data attributes
//	Note that this filter relies on wp_script_add_data() to ->get_data() from the script.
//	It also means that if any other scripts need datga- values, they'll need to be manually added here
///

function vnmContact_addDataAttributes($tag, $handle) {

	//	Convertr data-url

	$dataURL = wp_scripts()->get_data($handle, 'cvr-data-url');
	$dataID = wp_scripts()->get_data($handle, 'cvr-data-id');

	//	If there's a data-url (shouldn't be used anymore)

	// if ($dataURL) {
	// 	//	Replace the ID of the JS tag with a unique one required by the Convertr script

	// 	$tag = str_replace('vnm-convertr-js', 'cvtr-form-render', $tag);

	// 	//	Now add the data URL
		
	// 	$tag = str_replace('></', ' data-url="' . esc_attr($dataURL) . '" data-target="cvtr-form-container"></', $tag);
	// }

	//	If there's a data-id (for the JS implementation of Convertr)

	if ($dataID) {
		//	Replace the ID of the JS tag with a unique one required by the Convertr script

		$tag = str_replace('vnm-convertr-js', $dataID, $tag);

		//	Now add the data-target
		
		$tag = str_replace('></', ' data-target="cvtr-form-container"></', $tag);
	}

	return $tag;
}

add_filter('script_loader_tag', 'vnmContact_addDataAttributes', 10, 2);

///
//	Add action to ensure the return-path is set as the from address (as some mail servers will apparently reject it otherwise)
///

function vnmContact_filterReturnPath($phpmailer) {
	//	Set the Sender (return-path) if it is not already set
	
	$phpmailer->Sender = $phpmailer->From;
}

add_action('phpmailer_init', 'vnmContact_filterReturnPath', 10, 1);

?>